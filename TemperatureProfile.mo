within receiver;
package TemperatureProfile

record Pipe "Data sheet of a pipe"
  parameter Modelica.SIunits.Length R1 "Inner tube radius";
  parameter Modelica.SIunits.Length R2 "Outer tube radius";
  parameter Modelica.SIunits.ThermalConductivity k "Thermal conductivity of the pipe";
  parameter Integer nsum "Number of terms to approximate the infinite Fourier series";
  parameter Integer nr "Number of radial nodes";
  parameter Integer nphi "Number of circumferential nodes";
end Pipe;

function gn "gn(rr) = gn(a,rr,Bi1,n)"
  input Real a;
  input Real rr;
  input Real Bi1;
  input Integer n;
  output Real gn;
algorithm
  gn := rr^n + a^(2*n)*(n - Bi1)/((n + Bi1)*rr^n);
end gn;

function gpn "gpn(1) = gpn(a, Bi1, n)"
  input Real a;
  input Real Bi1;
  input Integer n;
  output Real gpn;
algorithm
  gpn := n*(1 - a^(2*n)*(n - Bi1)/(n + Bi1));
end gpn;

function PipeWallTemperature "T[nr,nphi] = PipeWallTemperature(pipe,h1,h2,Tinf1,Tinf2,q0)"
  extends Modelica.Icons.Function;
  import np = Modelica.Constants;
  import Modelica.Math.*;
  input Receiver.TemperatureProfile.Pipe pipe "Pipe record";
  input Modelica.SIunits.CoefficientOfHeatTransfer h1 "Internal heat transfer coefficient";
  input Modelica.SIunits.CoefficientOfHeatTransfer h2 "External heat transfer coefficient";
  input Modelica.SIunits.Temperature Tinf1 "Fluid bulk temperature";
  input Modelica.SIunits.Temperature Tinf2 "Ambient temperature";
  input Modelica.SIunits.HeatFlux q0 "Incoming heat flux";
  output Modelica.SIunits.Temperature Tpipe[pipe.nr,pipe.nphi] "Pipe temperature profile";
protected
  Real a "Dimensionless radius";
  Real Bi1, Bi2;
  Real TTinf2;
  Real r[pipe.nr];
  Real phi[pipe.nphi];
  Real A1, A2, A3, an, bn;
  Real anbn[pipe.nsum];
algorithm
  a := pipe.R1/pipe.R2;
  Bi1 := h1 * pipe.R1 / pipe.k;
  Bi2 := h2 * pipe.R2 / pipe.k;
  TTinf2 := (Tinf2 - Tinf1) * pipe.k / (q0 * pipe.R2);
  r := linspace(a, 1, pipe.nr) "radial nodes";
  phi := linspace(0, 2 * np.pi, pipe.nphi) "circumferential nodes";
  for i in 1:pipe.nr loop
    for j in 1:pipe.nphi loop
      A1 := (1. + np.pi * Bi2 * TTinf2) * (1. + Bi1 * log(r[i] / a)) / (np.pi * (Bi1 + Bi2 * (1. - Bi1 * log(a))));
      A2 := gn(a,r[i],Bi1,1) * cos(phi[j]) / (2. * (gpn(a,Bi1,1) + Bi2 * gn(a,1.,Bi1,1)));
      for n in 1:pipe.nsum loop
        an := ((-1)^n) * gn(a, r[i], Bi1, 2 * n) * cos(2 * n * phi[j]);
        bn := (1 - 4 * n^2) * (gpn(a, Bi1, 2 * n) + Bi2 * gn(a, 1., Bi1, 2 * n));
        anbn[n] := an / bn;
      end for;
      A3 := 2 / np.pi * sum(anbn);
      Tpipe[i,j] := Tinf1 + (q0 * pipe.R2)/pipe.k*(A1 + A2 + A3);
    end for;
  end for;
end PipeWallTemperature;

model Test  
  parameter Modelica.SIunits.CoefficientOfHeatTransfer h1 = 5925 "Internal heat transfer coefficient";
  parameter Modelica.SIunits.CoefficientOfHeatTransfer h2 = 0 "External heat transfer coefficient";
  parameter Modelica.SIunits.Temperature Tinf1 = 303.15 "Fluid bulk temperature";
  parameter Modelica.SIunits.Temperature Tinf2 = 298.15 "Ambient temperature";
  parameter Modelica.SIunits.HeatFlux q0 = 1e5 "Incoming heat flux";

  Receiver.TemperatureProfile.Pipe pipe1(
    R1 = 0.2,
    R2 = 0.4,
    k = 237,
    nsum = 10,
    nr = 10,
    nphi = 10);
  Modelica.SIunits.Temperature T[pipe1.nr,pipe1.nphi] "Pipe temperature profile";
//  output Boolean success1 "= true if writing to Test_RealMatrix_v4.mat is successful";
equation
  T = PipeWallTemperature(pipe1,h1,h2,Tinf1,Tinf2,q0);
//  when initial() then
//     success1 = Modelica.Utilities.Streams.writeRealMatrix("Test_RealMatrix_v4.mat", "Matrix_A", T);
//  end when;
end Test;

end TemperatureProfile;
