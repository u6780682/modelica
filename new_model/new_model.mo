package new_model

  function g
    input Real r;
    input Real n;
    input Real a;
    input Real Bi;
    output Real y;
  algorithm
  y := r^n + a^(2*n)*(n - Bi)/(n + Bi)*r^(-n);
  end g;
  function gp
    input Real r;
    input Real n;
    input Real a;
    input Real Bi;
    output Real y;
  algorithm
  y := n*(r^(n-1) - a^(2*n)*(n - Bi)/(n + Bi)*r^(-n-1));
  end gp;

  model PipeSection
  import Modelica.Math.*;
    import Modelica.SIunits.Conversions.*;
    replaceable package Medium = SolarTherm.Media.MoltenSalt.MoltenSalt_utilities;
  
  protected
    parameter SI.Height H_rcv = 10.5 "Receiver height"; //Gemasolar solar power tower https://doi.org/10.1016/j.solener.2015.12.055
    parameter Modelica.SIunits.Radius Ri = 21e-3 "Inner tube radius";
    parameter Modelica.SIunits.Radius Ro = 22.5e-3 "Outer tube radius";
    parameter Real Pi = Modelica.Constants.pi;
    parameter Real sigma = Modelica.Constants.sigma "Steffan-Boltzmann constant";
    //parameter Modelica.SIunits.HeaTinlux q0 = 0.9470e6 "Incident heat flux";
    parameter Modelica.SIunits.Efficiency ab = 0.93 "Coating absorptance";
    parameter Modelica.SIunits.Efficiency em = 0.87 "Coating emmisivity";
    parameter Modelica.SIunits.Temperature Tamb = from_degC(35);
    parameter Modelica.SIunits.Temperature Tsky = from_degC(30);
    parameter Modelica.SIunits.Efficiency em_sky = 0.895 "Sky emmisivity";
    parameter Modelica.SIunits.Efficiency em_gr = 0.955 "Ground emmisivity";
    parameter Modelica.SIunits.Temperature Ta = ((em_sky * Tsky ^ 4 + em_gr * Tamb ^ 4) / (em_gr + em_sky)) ^ 0.25;
    //parameter Modelica.SIunits.ThermalConductivity k = 0.0163914878 * from_degC(630) + 6.8703621459 "Thermal conductivity";
    //parameter Modelica.SIunits.Temperature Tin = from_degC(310);
    parameter Modelica.SIunits.Length dz = 0.42 "Longitude of the pipe segment";
    parameter Modelica.SIunits.MassFlowRate m_dot = 4.2;
    parameter Modelica.SIunits.CoefficientOfHeatTransfer hc = 10 "External coefficient of heat transfer due to forced convection";
    parameter Integer N_tb_pa = 1;
    Real Bif(start=3) "Internal Biot number";
    Real Nu "Internal Nusselt number";
    Real Re "Reynolds number";
    Real Pr "Prandtl number";
    Modelica.SIunits.Density rho;
    Modelica.SIunits.ThermalConductivity kf;
    Modelica.SIunits.DynamicViscosity muf;
    Modelica.SIunits.Velocity vf;
    Modelica.SIunits.SpecificHeatCapacity Cp;
    Modelica.SIunits.ThermalConductivity k;
    Real hrad;
    Real f;
    Real A0(start=500);
    Real A1(start=500);
    Real A2(start=500);
    Real B0(start=500);
    Real g1;
    Real g2;
    Real gp1;
    Real gp2;
  public
    Modelica.SIunits.CoefficientOfHeatTransfer hf "Internal coefficient of heat transfer due to forced convection";
    Modelica.SIunits.Temperature Ts;
    Modelica.SIunits.Temperature Ti;
    Modelica.SIunits.Temperature Tin;
    Modelica.SIunits.Temperature Tout;
    Modelica.SIunits.HeatFlux q0;
  equation
    k = 0.0163914878 * Ti + 6.8703621459;
    Tout = Tin + 2*Pi*k*B0*dz/(m_dot*Cp);
    hf = Nu * kf / (2 * Ri) / (1 + Nu * kf / (2 * Ri) * 8.808e-5);
    rho = Medium.rho_T(Tin);
    kf = Medium.lamda_T(Tin);
    muf = Medium.eta_T(Tin);
    vf = m_dot / (N_tb_pa * rho * Pi * Ri ^ 2);
    Cp = Medium.cp_T(Tin);
    Re = max(1, vf * rho * 2 * Ri / muf);
    Pr = max(0, muf * Cp / kf);
    f=(1.82*log10(Re)-1.64)^(-2);
    Nu = (f/8)*(Re-1000)*Pr/(1+12.7*(f/8)^0.5*((Pr^0.66)-1));
    Bif = hf * Ri / k;
    B0 = Bif * (A0 - Tin) / (1 - Bif * log(Ri));
    g1 = g(Ro, 1, Ri, Bif);
    g2 = g(Ro, 2, Ri, Bif);
    gp1 = gp(Ro, 1, Ri, Bif);
    gp2 = gp(Ro, 2, Ri, Bif);
    -((ab*q0)/k) + (B0*Pi)/Ro + (hc*(2*A1*g1 + Pi*(A0 - Ta) + B0*Pi*log(Ro)))/(2*k) + (1/(1680*k))*(em*sigma*(6720*A0^3*A1*g1 + 840*A0^4*Pi + 840*A0^2*(8*A1*A2*g1*g2 + 3*A1^2*g1^2*Pi + 3*A2^2*g2^2*Pi) + 56*A0*A1*g1*(168*A2^2*g2^2 + 5*A1*g1*(16*A1*g1 + 9*A2*g2*Pi)) + 3*(896*A1^3*A2*g1^3*g2 + 576*A1*A2^3*g1*g2^3 + 105*A1^4*g1^4*Pi + 420*A1^2*A2^2*g1^2*g2^2*Pi + 35*Pi*(3*A2^4*g2^4 - 8*Ta^4)) +  56*B0*log(Ro)*(8*A1*g1*(45*A0^2 + 10*A1^2*g1^2 + 30*A0*A2*g2 + 21*A2^2*g2^2) + 15*(4*A0^3 + 3*A1^2*A2*g1^2*g2 + 6*A0*(A1^2*g1^2 + A2^2*g2^2))*Pi + 15*B0*log(Ro)*(8*A1*g1*(3*A0 + A2*g2) + 3*(2*A0^2 + A1^2*g1^2 + A2^2*g2^2)*Pi + B0*log(Ro)*(8*A1*g1 + 4*A0*Pi + B0*Pi*log(Ro)))))) = 0;
    (A1*gp1*Pi)/2 - (ab*Pi*q0)/(4*k) + (hc*(A0 + (A2*g2)/3 + (A1*g1*Pi)/4 - Ta + B0*log(Ro)))/k + (1/(2520*k))*(em*sigma*(8*(21*(15*A0^4 + 60*A0^2*A1^2*g1^2 + 8*A1^4*g1^4) + 84*A0*A2*(5*A0^2 + 18*A1^2*g1^2)*g2 + 18*A2^2*(49*A0^2 + 38*A1^2*g1^2)*g2^2 + 324*A0*A2^3*g2^3 + 107*A2^4*g2^4) + 315*A1*g1*(8*A0^3 + 12*A0^2*A2*g2 + 4*A1^2*A2*g1^2*g2 + 3*A2^3*g2^3 + 6*A0*(A1^2*g1^2 + 2*A2^2*g2^2))*Pi - 2520*Ta^4 + 6*B0*log(Ro)*(48*(35*A0*(A0^2 + 2*A1^2*g1^2) + 7*A2*(5*A0^2 + 6*A1^2*g1^2)*g2 + 49*A0*A2^2*g2^2 + 9*A2^3*g2^3) + 315*A1*g1*(4*A0^2 + A1^2*g1^2 + 4*A0*A2*g2 + 2*A2^2*g2^2)*Pi + 14*B0*log(Ro)*(180*A0^2 + 84*A2^2*g2^2 + 30*A0*(4*A2*g2 + 3*A1*g1*Pi) + 15*A1*g1*(8*A1*g1 + 3*A2*g2*Pi) + 10*B0*log(Ro)*(12*A0 + 4*A2*g2 + 3*A1*g1*Pi + 3*B0*log(Ro)))))) = 0;
    (A2*gp2*Pi)/2 + (hc*(4*A1*g1 + 3*A2*g2*Pi))/(12*k) - (ab*q0)/(3*k) + (1/(5040*k))*(em*sigma*(252*A0^2*A1*g1*(112*A2*g2 + 15*A1*g1*Pi) + 1680*A0^3*(4*A1*g1 + 3*A2*g2*Pi) + A1*g1*(7296*A1^2*A2*g1^2*g2 + 6848*A2^3*g2^3 + 630*A1^3*g1^3*Pi + 2835*A1*A2^2*g1*g2^2*Pi) + 36*A0*(224*A1^3*g1^3 + 432*A1*A2^2*g1*g2^2 + 210*A1^2*A2*g1^2*g2*Pi + 105*A2^3*g2^3*Pi) + 36*B0*(224*A1^3*g1^3 + 432*A1*A2^2*g1*g2^2 + 210*A1^2*A2*g1^2*g2*Pi + 105*A2^3*g2^3*Pi + 14*A0*A1*g1*(112*A2*g2 + 15*A1*g1*Pi) + 140*A0^2*(4*A1*g1 + 3*A2*g2*Pi))*log(Ro) + 252*B0^2*(A1*g1*(112*A2*g2 + 15*A1*g1*Pi) + 20*A0*(4*A1*g1 + 3*A2*g2*Pi))*log(Ro)^2 + 1680*B0^3*(4*A1*g1 + 3*A2*g2*Pi)*log(Ro)^3)) = 0;
    Ts = A0 + B0 * log(Ro) + A1 * g1 + A2 * g2;
    Ti = A0 + B0 * log(Ri) + A1 * g(Ri, 1, Ri, Bif) + A2 * g(Ri, 2, Ri, Bif);
    hrad = em * sigma * (Ts ^ 2 + Ta ^ 2) * (Ts + Ta);
  end PipeSection;
  
  model ReceiverModel
    import SolarTherm.Media;
    import Modelica.Utilities.Streams.*;
    import Modelica.Utilities.Strings.*;
    import Modelica.SIunits.Conversions.*;	
    import Modelica.Constants.*;
    import Modelica.Math.*;
    
    Modelica.SIunits.HeatFlux q0[N];
    Modelica.SIunits.Temperature T_f[N];
    Modelica.SIunits.Temperature T_i[N];
    Modelica.SIunits.Temperature T_s[N];
  
  protected
    String line;
    String filepath = "/home/arfontalvo/gitlab/receiver/validation/uniform.csv";
    String filename = Modelica.Utilities.Files.loadResource(filepath);
    parameter SI.Height H_receiver = 10.5 "Receiver height"; //Gemasolar solar power tower https://doi.org/10.1016/j.solener.2015.12.055
    parameter Integer N = 225 "Number of segments";
    parameter Integer Npa = 9 "Number of panels per flow path";
    parameter Modelica.SIunits.Length d_z = 0.42 "Length of pipe segment";
    parameter Modelica.SIunits.Temperature Tin = from_degC(290);
    parameter Modelica.SIunits.MassFlowRate m_flow = 4.2;
    PipeSection[N] pipe(each dz = d_z);
  
  algorithm
    for i in 1:N loop
  	line := readLine(filename,i);
  	q0[i] := scanReal(line)*1000000;
    end for;
  
  equation
    pipe[1:end].Ti = T_i[1:end];
    pipe[1:end].Ts = T_s[1:end];
    pipe[1:end].Tin = T_f[1:end];
    pipe[1:end].q0 = q0[1:end];
    pipe[1].Tin = Tin;
    pipe[2:end].Tin = pipe[1:(end-1)].Tout;
  end ReceiverModel;
end new_model;