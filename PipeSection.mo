within receiver;
package PipeSection
model TwoCoefficients
import Modelica.Math.*;
  import Modelica.SIunits.Conversions.*;
  replaceable package Medium = SolarTherm.Media.MoltenSalt.MoltenSalt_utilities;

  parameter SI.Height H_rcv = 10.5 "Receiver height"; //Gemasolar solar power tower https://doi.org/10.1016/j.solener.2015.12.055
  parameter SI.Diameter D_rcv = 8.5 "Receiver diameter"; //Gemasolar solar power tower https://doi.org/10.1016/j.solener.2015.12.055
  parameter Modelica.SIunits.Radius Ri = 21e-3 "Inner tube radius";
  parameter Modelica.SIunits.Radius Ro = 22.5e-3 "Outer tube radius";
  parameter Real Pi = Modelica.Constants.pi;
  parameter Real sigma = Modelica.Constants.sigma "Steffan-Boltzmann constant";
  parameter Modelica.SIunits.Efficiency ab = 0.93 "Coating absorptance";
  parameter Modelica.SIunits.Efficiency em = 0.87 "Coating emmisivity";
  parameter Modelica.SIunits.Temperature Tamb = from_degC(35);
  parameter Modelica.SIunits.Temperature Tsky = from_degC(30);
  parameter Modelica.SIunits.Efficiency em_sky = 0.895 "Sky emmisivity";
  parameter Modelica.SIunits.Efficiency em_gr = 0.955 "Ground emmisivity";
  parameter Modelica.SIunits.Temperature Ta = ((em_sky * Tsky ^ 4 + em_gr * Tamb ^ 4) / (em_gr + em_sky)) ^ 0.25;
  parameter Modelica.SIunits.Length dz = 0.42 "Longitude of the pipe segment";
  parameter Modelica.SIunits.MassFlowRate m_dot = 4.2;
  parameter Integer N_tb_pa = 1;
  // ************ Parameters external heat transfer coefficients ***************//
  Modelica.SIunits.CoefficientOfHeatTransfer hc "External coefficient of heat transfer due to forced convection";
  parameter SI.Velocity v_wind = 5 "Wind speed";
  parameter Modelica.SIunits.SpecificHeatCapacity cp_a=1006 "Specific heat capacity of air at ambient temperature";
  parameter Modelica.SIunits.ThermalConductivity k_a=0.026 "Thermal conductivity of air at ambient temperature";
  parameter Modelica.SIunits.Density rho_a=1.184 "Density of air at ambient temperature";
  parameter Modelica.SIunits.DynamicViscosity mu_a=1.845e-5 "Dynamic viscosity at ambient temperature";
  parameter Modelica.SIunits.LinearExpansionCoefficient beta_a=0.003363 "Thermal expansion coefficient at ambient temperature";
  parameter Real Re_a = rho_a*v_wind*D_rcv/mu_a "Reynolds number of air at current wind speed";
  parameter Real Pr_a = cp_a*mu_a/k_a "Prandtl number of air at ambient temperature";
  parameter Modelica.SIunits.KinematicViscosity nu_a = mu_a/rho_a "Kinematic viscosity of air at ambient temperature";
  Real Gr_H "Grashof number for air at ambient temperature";
  Real Nu_nc "Nusselt number for air at ambient temperature";
  Modelica.SIunits.CoefficientOfHeatTransfer h_nc "Coefficient of heat transfer for ambient air due to natural convection";
  parameter Real Nu_fc = if v_wind > 0 then 0.3 + 0.62*Re_a^0.5*Pr_a^0.33/(1+(0.4/Pr_a)^(2/3))^(1/4)*(1+(Re_a/282000)^(5/8))^(4/5) else 0 "Nusselt number due to forced convection";
  parameter Modelica.SIunits.CoefficientOfHeatTransfer h_fc = Nu_fc*k_a/(D_rcv) "Coefficient of heat transfer for ambient air due to forced convection";
  // ************ End parameters external heat transfer coefficients ***************//

  Real Bif(start=3) "Internal Biot number";
  Real Nu "Internal Nusselt number";
  Real Re "Reynolds number";
  Real Pr "Prandtl number";
  Modelica.SIunits.Density rho "HTF density";
  Modelica.SIunits.ThermalConductivity kf "HTF thermal conductivity";
  Modelica.SIunits.DynamicViscosity muf "HTF dynamic viscosity";
  Modelica.SIunits.Velocity vf "HTF velocity";
  Modelica.SIunits.SpecificHeatCapacity Cp "HTF Specific heat capacity";
  Modelica.SIunits.ThermalConductivity k "Pipe wall thermal conductivity";
  Real f "Darcy friction factor";
  Real A0(start=500);
  Real A1(start=500);
  Real A2(start=500);
  Real B0(start=500);
  Real g1;
  Real g2;
  Real gp1;
  Real gp2;

  Modelica.SIunits.CoefficientOfHeatTransfer hf "Internal coefficient of heat transfer due to forced convection";
  Modelica.SIunits.Temperature Ts;
  Modelica.SIunits.Temperature Ti;
  Modelica.SIunits.Temperature Tin;
  Modelica.SIunits.Temperature Tout;
  Modelica.SIunits.SpecificEnthalpy hin;
  Modelica.SIunits.SpecificEnthalpy hout;
  Modelica.SIunits.SpecificEnthalpy hm;
  Modelica.SIunits.Temperature Tm;
  Modelica.SIunits.HeatFlux q0;
algorithm
  hc := if v_wind > 0 then (h_nc^3.2 + h_fc^3.2)^(1/3.2) else h_nc;

equation
  k = 0.0163914878 * Ti + 6.8703621459;
  
  // **** Begin external convection  ******//
  Gr_H = Modelica.Constants.g_n*beta_a*(Ts-Tamb)*H_rcv/nu_a^2;
  Nu_nc = 0.098*Pi/2*(Gr_H^(1/3))*((Ts/Tamb)^(-0.14));
  h_nc = Nu_nc*k_a/(H_rcv);
  // **** End external convection  ******//
  
  hout = hin + 2*Pi*k*B0*dz/m_dot;
  hm = hin + Pi*k*B0*dz/m_dot;
  hin = Medium.h_T(Tin);
  hout = Medium.h_T(Tout);
  hm = Medium.h_T(Tm);
  hf = Nu * kf / (2 * Ri) / (1 + Nu * kf / (2 * Ri) * 8.808e-5);
  rho = Medium.rho_T(Tm);
  kf = Medium.lamda_T(Tm);
  muf = Medium.eta_T(Tm);
  vf = m_dot / (N_tb_pa * rho * Pi * Ri ^ 2);
  Cp = Medium.cp_T(Tm);
  Re = max(1, vf * rho * 2 * Ri / muf);
  Pr = max(0, muf * Cp / kf);
  f=(1.82*log10(Re)-1.64)^(-2);
  Nu = (f/8)*(Re-1000)*Pr/(1+12.7*(f/8)^0.5*((Pr^0.66)-1));
  Bif = hf * Ri / k;
  B0 = Bif * (A0 - Tm) / (1 - Bif * log(Ri));
  g1 = g(Ro, 1, Ri, Bif);
  g2 = g(Ro, 2, Ri, Bif);
  gp1 = gp(Ro, 1, Ri, Bif);
  gp2 = gp(Ro, 2, Ri, Bif);

-((ab*q0)/k) + (B0*Pi)/Ro + (hc*Pi*(A0 - Ta + B0*log(Ro)))/k + (em*Pi*sigma*(8*A0^4 + 24*A0^2*A1^2*g1^2 + 3*A1^4*g1^4 + 24*A0*A1^2*A2*g1^2*g2 + 12*A2^2*(2*A0^2 + A1^2*g1^2)*g2^2 + 3*A2^4*g2^4 - 8*Ta^4 + 
     8*B0*log(Ro)*(4*A0^3 + 3*A1^2*A2*g1^2*g2 + 6*A0*(A1^2*g1^2 + A2^2*g2^2) + B0*log(Ro)*(3*(2*A0^2 + A1^2*g1^2 + A2^2*g2^2) + B0*log(Ro)*(4*A0 + B0*log(Ro))))))/(8*k) = 0;

(A1*gp1*Pi)/2 + (A1*g1*hc*Pi)/(2*k) - (ab*Pi*q0)/(4*k) + (A1*em*g1*Pi*sigma*(8*A0^3 + 12*A0^2*A2*g2 + 4*A1^2*A2*g1^2*g2 + 3*A2^3*g2^3 + 6*A0*(A1^2*g1^2 + 2*A2^2*g2^2) + 
     2*B0*log(Ro)*(3*(4*A0^2 + A1^2*g1^2 + 4*A0*A2*g2 + 2*A2^2*g2^2) + 2*B0*log(Ro)*(6*A0 + 3*A2*g2 + 2*B0*log(Ro)))))/(4*k) = 0;

(A2*gp2*Pi)/2 + (A2*g2*hc*Pi)/(2*k) - (ab*q0)/(3*k) + (em*Pi*sigma*(12*A0^2*A1^2*g1^2 + 2*A1^4*g1^4 + 16*A0^3*A2*g2 + 9*A1^2*A2^2*g1^2*g2^2 + 12*A0*(2*A1^2*A2*g1^2*g2 + A2^3*g2^3) + 
     4*B0*log(Ro)*(6*A0*A1^2*g1^2 + 6*A2*(2*A0^2 + A1^2*g1^2)*g2 + 3*A2^3*g2^3 + B0*log(Ro)*(3*A1^2*g1^2 + 12*A0*A2*g2 + 4*A2*B0*g2*log(Ro)))))/(8*k) = 0;

  Ts = A0 + B0 * log(Ro) + A1 * g1 + A2 * g2;
  Ti = A0 + B0 * log(Ri) + A1 * g(Ri, 1, Ri, Bif) + A2 * g(Ri, 2, Ri, Bif);
end TwoCoefficients;
  
  model TwoCoefficientsAdiabatic
  import Modelica.Math.*;
    import Modelica.SIunits.Conversions.*;
    replaceable package Medium = SolarTherm.Media.MoltenSalt.MoltenSalt_utilities;
  
    parameter SI.Height H_rcv = 10.5 "Receiver height"; //Gemasolar solar power tower https://doi.org/10.1016/j.solener.2015.12.055
    parameter SI.Diameter D_rcv = 8.5 "Receiver diameter"; //Gemasolar solar power tower https://doi.org/10.1016/j.solener.2015.12.055
    parameter Modelica.SIunits.Radius Ri = 21e-3 "Inner tube radius";
    parameter Modelica.SIunits.Radius Ro = 22.5e-3 "Outer tube radius";
    parameter Real Pi = Modelica.Constants.pi;
    parameter Real sigma = Modelica.Constants.sigma "Steffan-Boltzmann constant";
    parameter Modelica.SIunits.Efficiency ab = 0.93 "Coating absorptance";
    parameter Modelica.SIunits.Efficiency em = 0.87 "Coating emmisivity";
    parameter Modelica.SIunits.Temperature Tamb = from_degC(35);
    parameter Modelica.SIunits.Temperature Tsky = from_degC(30);
    parameter Modelica.SIunits.Efficiency em_sky = 0.895 "Sky emmisivity";
    parameter Modelica.SIunits.Efficiency em_gr = 0.955 "Ground emmisivity";
    parameter Modelica.SIunits.Temperature Ta = ((em_sky * Tsky ^ 4 + em_gr * Tamb ^ 4) / (em_gr + em_sky)) ^ 0.25;
    parameter Modelica.SIunits.Length dz = 0.42 "Longitude of the pipe segment";
    parameter Modelica.SIunits.MassFlowRate m_dot = 4.2;
    parameter Integer N_tb_pa = 1;
    // ************ Parameters external heat transfer coefficients ***************//
    Modelica.SIunits.CoefficientOfHeatTransfer hc "External coefficient of heat transfer due to forced convection";
    parameter SI.Velocity v_wind = 5;
    parameter Modelica.SIunits.SpecificHeatCapacity cp_a=1006;
    parameter Modelica.SIunits.ThermalConductivity k_a=0.026;
    parameter Modelica.SIunits.Density rho_a=1.184;
    parameter Modelica.SIunits.DynamicViscosity mu_a=1.845e-5;
    parameter Modelica.SIunits.LinearExpansionCoefficient beta_a=0.003363;
    parameter Real Re_a = rho_a*v_wind*D_rcv/mu_a;
    parameter Real Pr_a = cp_a*mu_a/k_a;
    parameter Real nu_a = mu_a/rho_a;
    Real Gr_H;
    Real Nu_nc;
    Modelica.SIunits.CoefficientOfHeatTransfer h_nc;
    parameter Real Nu_fc = if v_wind > 0 then 0.3 + 0.62*Re_a^0.5*Pr_a^0.33/(1+(0.4/Pr_a)^(2/3))^(1/4)*(1+(Re_a/282000)^(5/8))^(4/5) else 0;
    parameter Modelica.SIunits.CoefficientOfHeatTransfer h_fc = Nu_fc*k_a/(D_rcv);
    // ************ End parameters external heat transfer coefficients ***************//
  
    Real Bif(start=3) "Internal Biot number";
    Real Nu "Internal Nusselt number";
    Real Re "Reynolds number";
    Real Pr "Prandtl number";
    Modelica.SIunits.Density rho;
    Modelica.SIunits.ThermalConductivity kf;
    Modelica.SIunits.DynamicViscosity muf;
    Modelica.SIunits.Velocity vf;
    Modelica.SIunits.SpecificHeatCapacity Cp;
    Modelica.SIunits.ThermalConductivity k;
    Real hrad;
    Real f;
    Real A0(start=500);
    Real A1(start=500);
    Real A2(start=500);
    Real B0(start=500);
    Real g1;
    Real g2;
    Real gp1;
    Real gp2;
  
    Modelica.SIunits.CoefficientOfHeatTransfer hf "Internal coefficient of heat transfer due to forced convection";
    Modelica.SIunits.Temperature Ts;
    Modelica.SIunits.Temperature Ti;
    Modelica.SIunits.Temperature Tin;
    Modelica.SIunits.Temperature Tout;
    Modelica.SIunits.SpecificEnthalpy hin;
    Modelica.SIunits.SpecificEnthalpy hout;
    Modelica.SIunits.Temperature Tm;
    Modelica.SIunits.HeatFlux q0;
  algorithm
    hc := if v_wind > 0 then (h_nc^3.2 + h_fc^3.2)^(1/3.2) else h_nc;
  
  equation
    k = 0.0163914878 * Ti + 6.8703621459;
    //Tout = Tin + 2*Pi*k*B0*dz/(m_dot*Cp);
    
    // **** Begin external convection  ******//
    Gr_H = Modelica.Constants.g_n*beta_a*(Ts-Tamb)*H_rcv/nu_a^2;
    Nu_nc = 0.098*Pi/2*(Gr_H^(1/3))*((Ts/Tamb)^(-0.14));
    h_nc = Nu_nc*k_a/(H_rcv);
    // **** End external convection  ******//
    
    hout = hin + 2*Pi*k*B0*dz/m_dot;
    hin = Medium.h_T(Tin);
    hout = Medium.h_T(Tout);
    Tm = Tin;//0.5*(Tin + Tout);
    hf = Nu * kf / (2 * Ri) / (1 + Nu * kf / (2 * Ri) * 8.808e-5);
    rho = Medium.rho_T(Tm);
    kf = Medium.lamda_T(Tm);
    muf = Medium.eta_T(Tm);
    vf = m_dot / (N_tb_pa * rho * Pi * Ri ^ 2);
    Cp = Medium.cp_T(Tm);
    Re = max(1, vf * rho * 2 * Ri / muf);
    Pr = max(0, muf * Cp / kf);
    f=(1.82*log10(Re)-1.64)^(-2);
    Nu = (f/8)*(Re-1000)*Pr/(1+12.7*(f/8)^0.5*((Pr^0.66)-1));
    Bif = hf * Ri / k;
    B0 = Bif * (A0 - Tm) / (1 - Bif * log(Ri));
    g1 = g(Ro, 1, Ri, Bif);
    g2 = g(Ro, 2, Ri, Bif);
    gp1 = gp(Ro, 1, Ri, Bif);
    gp2 = gp(Ro, 2, Ri, Bif);
  
  -((ab*q0)/k) + (B0*Pi)/Ro + (hc*(2*A1*g1 + Pi*(A0 - Ta) + B0*Pi*log(Ro)))/(2*k) + (1/(1680*k))*(em*sigma*(6720*A0^3*A1*g1 + 840*A0^4*Pi + 840*A0^2*(8*A1*A2*g1*g2 + 3*A1^2*g1^2*Pi + 3*A2^2*g2^2*Pi) + 56*A0*A1*g1*(168*A2^2*g2^2 + 5*A1*g1*(16*A1*g1 + 9*A2*g2*Pi)) + 3*(896*A1^3*A2*g1^3*g2 + 576*A1*A2^3*g1*g2^3 + 105*A1^4*g1^4*Pi + 420*A1^2*A2^2*g1^2*g2^2*Pi + 35*Pi*(3*A2^4*g2^4 - 8*Ta^4)) + 56*B0*log(Ro)*(8*A1*g1*(45*A0^2 + 10*A1^2*g1^2 + 30*A0*A2*g2 + 21*A2^2*g2^2) + 15*(4*A0^3 + 3*A1^2*A2*g1^2*g2 + 6*A0*(A1^2*g1^2 + A2^2*g2^2))*Pi + 15*B0*log(Ro)*(8*A1*g1*(3*A0 + A2*g2) + 3*(2*A0^2 + A1^2*g1^2 + A2^2*g2^2)*Pi + B0*log(Ro)*(8*A1*g1 + 4*A0*Pi + B0*Pi*log(Ro)))))) = 0;
  
  (A1*gp1*Pi)/2 - (ab*Pi*q0)/(4*k) + (hc*(A0 + (A2*g2)/3 + (A1*g1*Pi)/4 - Ta + B0*log(Ro)))/k + (1/(2520*k))*(em*sigma*(8*(21*(15*A0^4 + 60*A0^2*A1^2*g1^2 + 8*A1^4*g1^4) + 84*A0*A2*(5*A0^2 + 18*A1^2*g1^2)*g2 + 18*A2^2*(49*A0^2 + 38*A1^2*g1^2)*g2^2 + 324*A0*A2^3*g2^3 + 107*A2^4*g2^4) + 315*A1*g1*(8*A0^3 + 12*A0^2*A2*g2 + 4*A1^2*A2*g1^2*g2 + 3*A2^3*g2^3 + 6*A0*(A1^2*g1^2 + 2*A2^2*g2^2))*Pi - 2520*Ta^4 + 6*B0*log(Ro)*(48*(35*A0*(A0^2 + 2*A1^2*g1^2) + 7*A2*(5*A0^2 + 6*A1^2*g1^2)*g2 + 49*A0*A2^2*g2^2 + 9*A2^3*g2^3) + 315*A1*g1*(4*A0^2 + A1^2*g1^2 + 4*A0*A2*g2 + 2*A2^2*g2^2)*Pi + 14*B0*log(Ro)*(180*A0^2 + 84*A2^2*g2^2 + 30*A0*(4*A2*g2 + 3*A1*g1*Pi) + 15*A1*g1*(8*A1*g1 + 3*A2*g2*Pi) + 10*B0*log(Ro)*(12*A0 + 4*A2*g2 + 3*A1*g1*Pi + 3*B0*log(Ro)))))) = 0;
  
  (A2*gp2*Pi)/2 + (hc*(4*A1*g1 + 3*A2*g2*Pi))/(12*k) - (ab*q0)/(3*k) + (1/(5040*k))*(em*sigma*(252*A0^2*A1*g1*(112*A2*g2 + 15*A1*g1*Pi) + 1680*A0^3*(4*A1*g1 + 3*A2*g2*Pi) + A1*g1*(7296*A1^2*A2*g1^2*g2 + 6848*A2^3*g2^3 + 630*A1^3*g1^3*Pi + 2835*A1*A2^2*g1*g2^2*Pi) + 36*A0*(224*A1^3*g1^3 + 432*A1*A2^2*g1*g2^2 + 210*A1^2*A2*g1^2*g2*Pi + 105*A2^3*g2^3*Pi) + 36*B0*(224*A1^3*g1^3 + 432*A1*A2^2*g1*g2^2 + 210*A1^2*A2*g1^2*g2*Pi + 105*A2^3*g2^3*Pi + 14*A0*A1*g1*(112*A2*g2 + 15*A1*g1*Pi) + 140*A0^2*(4*A1*g1 + 3*A2*g2*Pi))*log(Ro) + 252*B0^2*(A1*g1*(112*A2*g2 + 15*A1*g1*Pi) + 20*A0*(4*A1*g1 + 3*A2*g2*Pi))*log(Ro)^2 + 1680*B0^3*(4*A1*g1 + 3*A2*g2*Pi)*log(Ro)^3)) = 0;
  
    Ts = A0 + B0 * log(Ro) + A1 * g1 + A2 * g2;
    Ti = A0 + B0 * log(Ri) + A1 * g(Ri, 1, Ri, Bif) + A2 * g(Ri, 2, Ri, Bif);
    hrad = em * sigma * (Ts ^ 2 + Ta ^ 2) * (Ts + Ta);
  end TwoCoefficientsAdiabatic;
  
  model ThreeCoefficients
  import Modelica.Math.*;
    import Modelica.SIunits.Conversions.*;
    replaceable package Medium = SolarTherm.Media.MoltenSalt.MoltenSalt_utilities;
  
    parameter SI.Height H_rcv = 10.5 "Receiver height"; //Gemasolar solar power tower https://doi.org/10.1016/j.solener.2015.12.055
    parameter SI.Diameter D_rcv = 8.5 "Receiver diameter"; //Gemasolar solar power tower https://doi.org/10.1016/j.solener.2015.12.055
    parameter Modelica.SIunits.Radius Ri = 21e-3 "Inner tube radius";
    parameter Modelica.SIunits.Radius Ro = 22.5e-3 "Outer tube radius";
    parameter Real Pi = Modelica.Constants.pi;
    parameter Real sigma = Modelica.Constants.sigma "Steffan-Boltzmann constant";
    parameter Modelica.SIunits.Efficiency ab = 0.93 "Coating absorptance";
    parameter Modelica.SIunits.Efficiency em = 0.87 "Coating emmisivity";
    parameter Modelica.SIunits.Temperature Tamb = from_degC(35);
    parameter Modelica.SIunits.Temperature Tsky = from_degC(30);
    parameter Modelica.SIunits.Efficiency em_sky = 0.895 "Sky emmisivity";
    parameter Modelica.SIunits.Efficiency em_gr = 0.955 "Ground emmisivity";
    parameter Modelica.SIunits.Temperature Ta = ((em_sky * Tsky ^ 4 + em_gr * Tamb ^ 4) / (em_gr + em_sky)) ^ 0.25;
    parameter Modelica.SIunits.Length dz = 0.42 "Longitude of the pipe segment";
    parameter Modelica.SIunits.MassFlowRate m_dot = 4.2;
    parameter Integer N_tb_pa = 1;
    // ************ Parameters external heat transfer coefficients ***************//
    Modelica.SIunits.CoefficientOfHeatTransfer hc "External coefficient of heat transfer due to forced convection";
    parameter SI.Velocity v_wind = 5 "Wind speed";
    parameter Modelica.SIunits.SpecificHeatCapacity cp_a=1006 "Specific heat capacity of air at ambient temperature";
    parameter Modelica.SIunits.ThermalConductivity k_a=0.026 "Thermal conductivity of air at ambient temperature";
    parameter Modelica.SIunits.Density rho_a=1.184 "Density of air at ambient temperature";
    parameter Modelica.SIunits.DynamicViscosity mu_a=1.845e-5 "Dynamic viscosity at ambient temperature";
    parameter Modelica.SIunits.LinearExpansionCoefficient beta_a=0.003363 "Thermal expansion coefficient at ambient temperature";
    parameter Real Re_a = rho_a*v_wind*D_rcv/mu_a "Reynolds number of air at current wind speed";
    parameter Real Pr_a = cp_a*mu_a/k_a "Prandtl number of air at ambient temperature";
    parameter Modelica.SIunits.KinematicViscosity nu_a = mu_a/rho_a "Kinematic viscosity of air at ambient temperature";
    Real Gr_H "Grashof number for air at ambient temperature";
    Real Nu_nc "Nusselt number for air at ambient temperature";
    Modelica.SIunits.CoefficientOfHeatTransfer h_nc "Coefficient of heat transfer for ambient air due to natural convection";
    parameter Real Nu_fc = if v_wind > 0 then 0.3 + 0.62*Re_a^0.5*Pr_a^0.33/(1+(0.4/Pr_a)^(2/3))^(1/4)*(1+(Re_a/282000)^(5/8))^(4/5) else 0 "Nusselt number due to forced convection";
    parameter Modelica.SIunits.CoefficientOfHeatTransfer h_fc = Nu_fc*k_a/(D_rcv) "Coefficient of heat transfer for ambient air due to forced convection";
    // ************ End parameters external heat transfer coefficients ***************//
  
    Real Bif(start=3) "Internal Biot number";
    Real Nu "Internal Nusselt number";
    Real Re "Reynolds number";
    Real Pr "Prandtl number";
    Modelica.SIunits.Density rho "HTF density";
    Modelica.SIunits.ThermalConductivity kf "HTF thermal conductivity";
    Modelica.SIunits.DynamicViscosity muf "HTF dynamic viscosity";
    Modelica.SIunits.Velocity vf "HTF velocity";
    Modelica.SIunits.SpecificHeatCapacity Cp "HTF Specific heat capacity";
    Modelica.SIunits.ThermalConductivity k "Pipe wall thermal conductivity";
    Real f "Darcy friction factor";
    Real A0(start=500);
    Real A1(start=500);
    Real A2(start=500);
    Real A3(start=500);
    Real B0(start=500);
    Real g1;
    Real g2;
    Real g3;
    Real gp1;
    Real gp2;
    Real gp3;
  
    Modelica.SIunits.CoefficientOfHeatTransfer hf "Internal coefficient of heat transfer due to forced convection";
    Modelica.SIunits.Temperature Ts;
    Modelica.SIunits.Temperature Ti;
    Modelica.SIunits.Temperature Tin;
    Modelica.SIunits.Temperature Tout;
    Modelica.SIunits.SpecificEnthalpy hin;
    Modelica.SIunits.SpecificEnthalpy hout;
    Modelica.SIunits.SpecificEnthalpy hm;
    Modelica.SIunits.Temperature Tm;
    Modelica.SIunits.HeatFlux q0;
  algorithm
    hc := if v_wind > 0 then (h_nc^3.2 + h_fc^3.2)^(1/3.2) else h_nc;
  
  equation
    k = 0.0163914878 * Ti + 6.8703621459;
    
    // **** Begin external convection  ******//
    Gr_H = Modelica.Constants.g_n*beta_a*(Ts-Tamb)*H_rcv/nu_a^2;
    Nu_nc = 0.098*Pi/2*(Gr_H^(1/3))*((Ts/Tamb)^(-0.14));
    h_nc = Nu_nc*k_a/(H_rcv);
    // **** End external convection  ******//
    
    hout = hin + 2*Pi*k*B0*dz/m_dot;
    hm = hin + Pi*k*B0*dz/m_dot;
    hin = Medium.h_T(Tin);
    hout = Medium.h_T(Tout);
    hm = Medium.h_T(Tm);
    hf = Nu * kf / (2 * Ri) / (1 + Nu * kf / (2 * Ri) * 8.808e-5);
    rho = Medium.rho_T(Tm);
    kf = Medium.lamda_T(Tm);
    muf = Medium.eta_T(Tm);
    vf = m_dot / (N_tb_pa * rho * Pi * Ri ^ 2);
    Cp = Medium.cp_T(Tm);
    Re = max(1, vf * rho * 2 * Ri / muf);
    Pr = max(0, muf * Cp / kf);
    f=(1.82*log10(Re)-1.64)^(-2);
    Nu = (f/8)*(Re-1000)*Pr/(1+12.7*(f/8)^0.5*((Pr^0.66)-1));
    Bif = hf * Ri / k;
    B0 = Bif * (A0 - Tm) / (1 - Bif * log(Ri));
    g1 = g(Ro, 1, Ri, Bif);
    g2 = g(Ro, 2, Ri, Bif);
    g3 = g(Ro, 3, Ri, Bif);
    gp1 = gp(Ro, 1, Ri, Bif);
    gp2 = gp(Ro, 2, Ri, Bif);
    gp3 = gp(Ro, 3, Ri, Bif);
  
  -((ab*q0)/k) + (B0*Pi)/Ro + (hc*Pi*(A0 - Ta + B0*log(Ro)))/k + (1/(8*k))*(em*Pi*sigma*(8*A0^4 + 3*(A1^4*g1^4 + 4*A1^2*A2^2*g1^2*g2^2 + A2^4*g2^4) + 4*A1*A3*g1*(A1^2*g1^2 + 3*A2^2*g2^2)*g3 + 
       12*A3^2*(A1^2*g1^2 + A2^2*g2^2)*g3^2 + 3*A3^4*g3^4 + 24*A0*A1*A2*g1*g2*(A1*g1 + 2*A3*g3) + 24*A0^2*(A1^2*g1^2 + A2^2*g2^2 + A3^2*g3^2) - 8*Ta^4 + 
       8*B0*log(Ro)*(4*A0^3 + 3*A1*A2*g1*g2*(A1*g1 + 2*A3*g3) + 6*A0*(A1^2*g1^2 + A2^2*g2^2 + A3^2*g3^2) + B0*log(Ro)*(3*(2*A0^2 + A1^2*g1^2 + A2^2*g2^2 + A3^2*g3^2) + B0*log(Ro)*(4*A0 + B0*log(Ro)))))) = 0;
  
  (A1*gp1*Pi)/2 + (A1*g1*hc*Pi)/(2*k) - (ab*Pi*q0)/(4*k) + (1/(4*k))*(em*Pi*sigma*(A1*g1*(8*A0^3 + 12*A0^2*A2*g2 + 4*A1^2*A2*g1^2*g2 + 3*A2^3*g2^3 + 6*A0*(A1^2*g1^2 + 2*A2^2*g2^2)) + 
       3*A3*(4*A0^2*A2*g2 + 3*A1^2*A2*g1^2*g2 + A2^3*g2^3 + 2*A0*(A1^2*g1^2 + A2^2*g2^2))*g3 + 6*A1*A3^2*g1*(2*A0 + A2*g2)*g3^2 + 3*A2*A3^3*g2*g3^3 + 
       2*B0*log(Ro)*(3*A1*g1*(4*A0^2 + A1^2*g1^2 + 4*A0*A2*g2 + 2*A2^2*g2^2) + 3*A3*(A1^2*g1^2 + A2*g2*(4*A0 + A2*g2))*g3 + 6*A1*A3^2*g1*g3^2 + 2*B0*log(Ro)*(6*A0*A1*g1 + 3*A2*g2*(A1*g1 + A3*g3) + 2*A1*B0*g1*log(Ro))))) = 0;
  
  (A2*gp2*Pi)/2 + (A2*g2*hc*Pi)/(2*k) - (ab*q0)/(3*k) + (1/(8*k))*(em*Pi*sigma*(12*A0^2*A1^2*g1^2 + 2*A1^4*g1^4 + 16*A0^3*A2*g2 + 24*A0*A1^2*A2*g1^2*g2 + 9*A1^2*A2^2*g1^2*g2^2 + 12*A0*A2^3*g2^3 + 
       6*A1*A3*g1*(4*A0^2 + A1^2*g1^2 + 4*A0*A2*g2 + 3*A2^2*g2^2)*g3 + 3*A3^2*(2*A1^2*g1^2 + A2*g2*(8*A0 + A2*g2))*g3^2 + 6*A1*A3^3*g1*g3^3 + 
       4*B0*log(Ro)*(3*(4*A0^2*A2*g2 + 2*A0*A1*g1*(A1*g1 + 2*A3*g3) + A2*g2*(2*A1^2*g1^2 + A2^2*g2^2 + 2*A1*A3*g1*g3 + 2*A3^2*g3^2)) + B0*log(Ro)*(12*A0*A2*g2 + 3*A1*g1*(A1*g1 + 2*A3*g3) + 4*A2*B0*g2*log(Ro))))) = 0;
  
  (A3*gp3*Pi)/2 + (A3*g3*hc*Pi)/(2*k) + (1/(4*k))*(em*Pi*sigma*(A1*g1*(2*A0*A1^2*g1^2 + 3*A2*(4*A0^2 + A1^2*g1^2)*g2 + 6*A0*A2^2*g2^2 + 3*A2^3*g2^3) + A3*(8*A0^3 + 6*A1^2*A2*g1^2*g2 + A2^3*g2^3 + 12*A0*(A1^2*g1^2 + A2^2*g2^2))*
        g3 + 9*A1*A2*A3^2*g1*g2*g3^2 + 6*A0*A3^3*g3^3 + 2*B0*log(Ro)*(A1^3*g1^3 + 3*A1*A2*g1*g2*(4*A0 + A2*g2) + 6*A3*(2*A0^2 + A1^2*g1^2 + A2^2*g2^2)*g3 + 3*A3^3*g3^3 + 
         2*B0*log(Ro)*(3*A1*A2*g1*g2 + 6*A0*A3*g3 + 2*A3*B0*g3*log(Ro))))) = 0;
  
    Ts = A0 + B0 * log(Ro) + A1 * g1 + A2 * g2  + A3 * g3;
    Ti = A0 + B0 * log(Ri) + A1 * g(Ri, 1, Ri, Bif) + A2 * g(Ri, 2, Ri, Bif) + A3 * g(Ri, 3, Ri, Bif);
  end ThreeCoefficients;
  
  model FourCoefficients
  import Modelica.Math.*;
    import Modelica.SIunits.Conversions.*;
    replaceable package Medium = SolarTherm.Media.MoltenSalt.MoltenSalt_utilities;
  
    parameter SI.Height H_rcv = 10.5 "Receiver height"; //Gemasolar solar power tower https://doi.org/10.1016/j.solener.2015.12.055
    parameter SI.Diameter D_rcv = 8.5 "Receiver diameter"; //Gemasolar solar power tower https://doi.org/10.1016/j.solener.2015.12.055
    parameter Modelica.SIunits.Radius Ri = 21e-3 "Inner tube radius";
    parameter Modelica.SIunits.Radius Ro = 22.5e-3 "Outer tube radius";
    parameter Real Pi = Modelica.Constants.pi;
    parameter Real sigma = Modelica.Constants.sigma "Steffan-Boltzmann constant";
    parameter Modelica.SIunits.Efficiency ab = 0.93 "Coating absorptance";
    parameter Modelica.SIunits.Efficiency em = 0.87 "Coating emmisivity";
    parameter Modelica.SIunits.Temperature Tamb = from_degC(35);
    parameter Modelica.SIunits.Temperature Tsky = from_degC(30);
    parameter Modelica.SIunits.Efficiency em_sky = 0.895 "Sky emmisivity";
    parameter Modelica.SIunits.Efficiency em_gr = 0.955 "Ground emmisivity";
    parameter Modelica.SIunits.Temperature Ta = ((em_sky * Tsky ^ 4 + em_gr * Tamb ^ 4) / (em_gr + em_sky)) ^ 0.25;
    parameter Modelica.SIunits.Length dz = 0.42 "Longitude of the pipe segment";
    parameter Modelica.SIunits.MassFlowRate m_dot = 4.2;
    parameter Integer N_tb_pa = 1;
    // ************ Parameters external heat transfer coefficients ***************//
    Modelica.SIunits.CoefficientOfHeatTransfer hc "External coefficient of heat transfer due to forced convection";
    parameter SI.Velocity v_wind = 5 "Wind speed";
    parameter Modelica.SIunits.SpecificHeatCapacity cp_a=1006 "Specific heat capacity of air at ambient temperature";
    parameter Modelica.SIunits.ThermalConductivity k_a=0.026 "Thermal conductivity of air at ambient temperature";
    parameter Modelica.SIunits.Density rho_a=1.184 "Density of air at ambient temperature";
    parameter Modelica.SIunits.DynamicViscosity mu_a=1.845e-5 "Dynamic viscosity at ambient temperature";
    parameter Modelica.SIunits.LinearExpansionCoefficient beta_a=0.003363 "Thermal expansion coefficient at ambient temperature";
    parameter Real Re_a = rho_a*v_wind*D_rcv/mu_a "Reynolds number of air at current wind speed";
    parameter Real Pr_a = cp_a*mu_a/k_a "Prandtl number of air at ambient temperature";
    parameter Modelica.SIunits.KinematicViscosity nu_a = mu_a/rho_a "Kinematic viscosity of air at ambient temperature";
    Real Gr_H "Grashof number for air at ambient temperature";
    Real Nu_nc "Nusselt number for air at ambient temperature";
    Modelica.SIunits.CoefficientOfHeatTransfer h_nc "Coefficient of heat transfer for ambient air due to natural convection";
    parameter Real Nu_fc = if v_wind > 0 then 0.3 + 0.62*Re_a^0.5*Pr_a^0.33/(1+(0.4/Pr_a)^(2/3))^(1/4)*(1+(Re_a/282000)^(5/8))^(4/5) else 0 "Nusselt number due to forced convection";
    parameter Modelica.SIunits.CoefficientOfHeatTransfer h_fc = Nu_fc*k_a/(D_rcv) "Coefficient of heat transfer for ambient air due to forced convection";
    // ************ End parameters external heat transfer coefficients ***************//
  
    Real Bif(start=3) "Internal Biot number";
    Real Nu "Internal Nusselt number";
    Real Re "Reynolds number";
    Real Pr "Prandtl number";
    Modelica.SIunits.Density rho "HTF density";
    Modelica.SIunits.ThermalConductivity kf "HTF thermal conductivity";
    Modelica.SIunits.DynamicViscosity muf "HTF dynamic viscosity";
    Modelica.SIunits.Velocity vf "HTF velocity";
    Modelica.SIunits.SpecificHeatCapacity Cp "HTF Specific heat capacity";
    Modelica.SIunits.ThermalConductivity k "Pipe wall thermal conductivity";
    Real f "Darcy friction factor";
    Real A0(start=500);
    Real A1(start=500);
    Real A2(start=500);
    Real A3(start=500);
    Real A4(start=500);
    Real B0(start=500);
    Real g1;
    Real g2;
    Real g3;
    Real g4;
    Real gp1;
    Real gp2;
    Real gp3;
    Real gp4;
  
    Modelica.SIunits.CoefficientOfHeatTransfer hf "Internal coefficient of heat transfer due to forced convection";
    Modelica.SIunits.Temperature Ts;
    Modelica.SIunits.Temperature Ti;
    Modelica.SIunits.Temperature Tin;
    Modelica.SIunits.Temperature Tout;
    Modelica.SIunits.SpecificEnthalpy hin;
    Modelica.SIunits.SpecificEnthalpy hout;
    Modelica.SIunits.SpecificEnthalpy hm;
    Modelica.SIunits.Temperature Tm;
    Modelica.SIunits.HeatFlux q0;
  algorithm
    hc := if v_wind > 0 then (h_nc^3.2 + h_fc^3.2)^(1/3.2) else h_nc;
  
  equation
    k = 0.0163914878 * Ti + 6.8703621459;
    
    // **** Begin external convection  ******//
    Gr_H = Modelica.Constants.g_n*beta_a*(Ts-Tamb)*H_rcv/nu_a^2;
    Nu_nc = 0.098*Pi/2*(Gr_H^(1/3))*((Ts/Tamb)^(-0.14));
    h_nc = Nu_nc*k_a/(H_rcv);
    // **** End external convection  ******//
    
    hout = hin + 2*Pi*k*B0*dz/m_dot;
    hm = hin + Pi*k*B0*dz/m_dot;
    hin = Medium.h_T(Tin);
    hout = Medium.h_T(Tout);
    hm = Medium.h_T(Tm);
    hf = Nu * kf / (2 * Ri) / (1 + Nu * kf / (2 * Ri) * 8.808e-5);
    rho = Medium.rho_T(Tm);
    kf = Medium.lamda_T(Tm);
    muf = Medium.eta_T(Tm);
    vf = m_dot / (N_tb_pa * rho * Pi * Ri ^ 2);
    Cp = Medium.cp_T(Tm);
    Re = max(1, vf * rho * 2 * Ri / muf);
    Pr = max(0, muf * Cp / kf);
    f=(1.82*log10(Re)-1.64)^(-2);
    Nu = (f/8)*(Re-1000)*Pr/(1+12.7*(f/8)^0.5*((Pr^0.66)-1));
    Bif = hf * Ri / k;
    B0 = Bif * (A0 - Tm) / (1 - Bif * log(Ri));
    g1 = g(Ro, 1, Ri, Bif);
    g2 = g(Ro, 2, Ri, Bif);
    g3 = g(Ro, 3, Ri, Bif);
    g4 = g(Ro, 4, Ri, Bif);
    gp1 = gp(Ro, 1, Ri, Bif);
    gp2 = gp(Ro, 2, Ri, Bif);
    gp3 = gp(Ro, 3, Ri, Bif);
    gp4 = gp(Ro, 4, Ri, Bif);
  
  -((ab*q0)/k) + (B0*Pi)/Ro + (hc*Pi*(A0 - Ta + B0*log(Ro)))/k + (1/(8*k))*(em*Pi*sigma*(8*A0^4 + 3*A1^4*g1^4 + 4*A1^3*A3*g1^3*g3 + 12*A1*A2*A3*g1*g2*g3*(A2*g2 + 2*A4*g4) + 
       24*A0*(A1*A2*g1*g2*(A1*g1 + 2*A3*g3) + A4*(A2^2*g2^2 + 2*A1*A3*g1*g3)*g4) + 24*A0^2*(A1^2*g1^2 + A2^2*g2^2 + A3^2*g3^2 + A4^2*g4^2) + 12*A1^2*g1^2*(A2^2*g2^2 + A3^2*g3^2 + A2*A4*g2*g4 + A4^2*g4^2) + 
       3*(A2^4*g2^4 + A3^4*g3^4 + 4*A2*A3^2*A4*g2*g3^2*g4 + 4*A3^2*A4^2*g3^2*g4^2 + A4^4*g4^4 + 4*A2^2*g2^2*(A3^2*g3^2 + A4^2*g4^2)) - 8*Ta^4 + 
       8*B0*log(Ro)*(4*A0^3 + 3*A1*A2*g1*g2*(A1*g1 + 2*A3*g3) + 3*A4*(A2^2*g2^2 + 2*A1*A3*g1*g3)*g4 + 6*A0*(A1^2*g1^2 + A2^2*g2^2 + A3^2*g3^2 + A4^2*g4^2) + 
         B0*log(Ro)*(3*(2*A0^2 + A1^2*g1^2 + A2^2*g2^2 + A3^2*g3^2 + A4^2*g4^2) + B0*log(Ro)*(4*A0 + B0*log(Ro)))))) = 0;
  
  (A1*gp1*Pi)/2 + (A1*g1*hc*Pi)/(2*k) - (ab*Pi*q0)/(4*k) + (1/(4*k))*(em*Pi*sigma*(8*A0^3*A1*g1 + 9*A1^2*A3*g1^2*g3*(A2*g2 + A4*g4) + A1^3*g1^3*(4*A2*g2 + A4*g4) + 
       3*A3*g3*(A2*g2 + A4*g4)*(A2^2*g2^2 + A3^2*g3^2 + A2*A4*g2*g4 + A4^2*g4^2) + 12*A0^2*(A1*A2*g1*g2 + A3*g3*(A2*g2 + A4*g4)) + 3*A1*g1*(A2^3*g2^3 + 2*A2^2*A4*g2^2*g4 + A3^2*A4*g3^2*g4 + 2*A2*g2*(A3^2*g3^2 + A4^2*g4^2)) + 
       6*A0*(A1^3*g1^3 + A1^2*A3*g1^2*g3 + A2*A3*g2*g3*(A2*g2 + 2*A4*g4) + 2*A1*g1*(A2^2*g2^2 + A3^2*g3^2 + A2*A4*g2*g4 + A4^2*g4^2)) + 
       2*B0*log(Ro)*(3*(4*A0^2*A1*g1 + A1^3*g1^3 + A1^2*A3*g1^2*g3 + A2*A3*g2*g3*(A2*g2 + 2*A4*g4) + 2*A1*g1*(A2^2*g2^2 + A3^2*g3^2 + A2*A4*g2*g4 + A4^2*g4^2) + 4*A0*(A1*A2*g1*g2 + A3*g3*(A2*g2 + A4*g4))) + 
         2*B0*log(Ro)*(3*(2*A0*A1*g1 + A1*A2*g1*g2 + A2*A3*g2*g3 + A3*A4*g3*g4) + 2*A1*B0*g1*log(Ro))))) = 0;
  
  (A2*gp2*Pi)/2 + (A2*g2*hc*Pi)/(2*k) - (ab*q0)/(3*k) + (1/(8*k))*(em*Pi*sigma*(2*A1^4*g1^4 + 16*A0^3*A2*g2 + 9*A1^2*A2^2*g1^2*g2^2 + 6*A1^3*A3*g1^3*g3 + 18*A1*A2^2*A3*g1*g2^2*g3 + 6*A1^2*A3^2*g1^2*g3^2 + 
       3*A2^2*A3^2*g2^2*g3^2 + 6*A1*A3^3*g1*g3^3 + 4*A2*A4*g2*(2*A2^2*g2^2 + 3*(A1*g1 + A3*g3)^2)*g4 + 3*A4^2*(2*A1^2*g1^2 + 4*A1*A3*g1*g3 + A3^2*g3^2)*g4^2 + 6*A2*A4^3*g2*g4^3 + 
       12*A0^2*(A1*g1*(A1*g1 + 2*A3*g3) + 2*A2*A4*g2*g4) + 12*A0*(A2*g2*(2*A1^2*g1^2 + A2^2*g2^2 + 2*A1*A3*g1*g3 + 2*A3^2*g3^2) + A4*(A1*g1 + A3*g3)^2*g4 + 2*A2*A4^2*g2*g4^2) + 
       4*B0*log(Ro)*(3*(4*A0^2*A2*g2 + 2*A0*A1*g1*(A1*g1 + 2*A3*g3) + A2*g2*(2*A1^2*g1^2 + A2^2*g2^2 + 2*A1*A3*g1*g3 + 2*A3^2*g3^2) + 4*A0*A2*A4*g2*g4 + A4*(A1*g1 + A3*g3)^2*g4 + 2*A2*A4^2*g2*g4^2) + 
         B0*log(Ro)*(3*A1*g1*(A1*g1 + 2*A3*g3) + 6*A2*g2*(2*A0 + A4*g4) + 4*A2*B0*g2*log(Ro))))) = 0;
  
  (A3*gp3*Pi)/2 + (A3*g3*hc*Pi)/(2*k) + (1/(4*k))*(em*Pi*sigma*(8*A0^3*A3*g3 + 12*A0^2*A1*g1*(A2*g2 + A4*g4) + 3*A1^3*g1^3*(A2*g2 + A4*g4) + 3*A1^2*A3*g1^2*g3*(2*A2*g2 + A4*g4) + 
       3*A1*g1*(A2*g2 + A4*g4)*(A2^2*g2^2 + 3*A3^2*g3^2 + A2*A4*g2*g4 + A4^2*g4^2) + A2*A3*g2*g3*(A2^2*g2^2 + 6*A2*A4*g2*g4 + 3*A4^2*g4^2) + 
       2*A0*(A1^3*g1^3 + 6*A1^2*A3*g1^2*g3 + 3*A1*A2*g1*g2*(A2*g2 + 2*A4*g4) + 3*A3*g3*(2*A2^2*g2^2 + A3^2*g3^2 + 2*A2*A4*g2*g4 + 2*A4^2*g4^2)) + 
       2*B0*log(Ro)*(A1^3*g1^3 + 6*A1^2*A3*g1^2*g3 + 3*A3*g3*(4*A0^2 + 2*A2^2*g2^2 + A3^2*g3^2 + 2*A2*A4*g2*g4 + 2*A4^2*g4^2) + 3*A1*g1*(4*A0*(A2*g2 + A4*g4) + A2*g2*(A2*g2 + 2*A4*g4)) + 
         2*B0*log(Ro)*(6*A0*A3*g3 + 3*A1*g1*(A2*g2 + A4*g4) + 2*A3*B0*g3*log(Ro))))) = 0;
  
  (A4*gp4*Pi)/2 + (A4*g4*hc*Pi)/(2*k) + (ab*q0)/(15*k) + (1/(16*k))*(em*Pi*sigma*(A1^4*g1^4 + 12*A1^3*A3*g1^3*g3 + 4*A2*g2*(6*A0^2*A2*g2 + A2^3*g2^3 + 3*A3^2*(2*A0 + A2*g2)*g3^2) + 
       4*A4*(8*A0^3 + 12*A0*A2^2*g2^2 + 3*A3^2*(4*A0 + A2*g2)*g3^2)*g4 + 18*A2^2*A4^2*g2^2*g4^2 + 24*A0*A4^3*g4^3 + 12*A1*A3*g1*g3*(4*A0^2 + 4*A0*A2*g2 + 2*A2^2*g2^2 + A3^2*g3^2 + 4*A2*A4*g2*g4 + 3*A4^2*g4^2) + 
       6*A1^2*g1^2*(A3^2*g3^2 + 2*(2*A0 + A2*g2)*(A2*g2 + 2*A4*g4)) + 8*B0*log(Ro)*(3*(2*A1*A3*g1*(2*A0 + A2*g2)*g3 + A2*g2*(2*A0*A2*g2 + A3^2*g3^2) + 2*A4*(2*A0^2 + A2^2*g2^2 + A3^2*g3^2)*g4 + A4^3*g4^3 + 
           A1^2*g1^2*(A2*g2 + 2*A4*g4)) + B0*log(Ro)*(3*A2^2*g2^2 + 6*A1*A3*g1*g3 + 12*A0*A4*g4 + 4*A4*B0*g4*log(Ro))))) = 0;
  
    Ts = A0 + B0 * log(Ro) + A1 * g1 + A2 * g2  + A3 * g3 + A4 * g4;
    Ti = A0 + B0 * log(Ri) + A1 * g(Ri, 1, Ri, Bif) + A2 * g(Ri, 2, Ri, Bif) + A3 * g(Ri, 3, Ri, Bif) + A4 * g(Ri, 4, Ri, Bif);
  end FourCoefficients;
  
  model FiveCoefficients
    import Modelica.Math.*;
    import Modelica.SIunits.Conversions.*;
    replaceable package Medium = SolarTherm.Media.MoltenSalt.MoltenSalt_utilities;
  
    parameter SI.Height H_rcv = 10.5 "Receiver height"; //Gemasolar solar power tower https://doi.org/10.1016/j.solener.2015.12.055
    parameter SI.Diameter D_rcv = 8.5 "Receiver diameter"; //Gemasolar solar power tower https://doi.org/10.1016/j.solener.2015.12.055
    parameter Modelica.SIunits.Radius Ri = 21e-3 "Inner tube radius";
    parameter Modelica.SIunits.Radius Ro = 22.5e-3 "Outer tube radius";
    parameter Real Pi = Modelica.Constants.pi;
    parameter Real sigma = Modelica.Constants.sigma "Steffan-Boltzmann constant";
    parameter Modelica.SIunits.Efficiency ab = 0.93 "Coating absorptance";
    parameter Modelica.SIunits.Efficiency em = 0.87 "Coating emmisivity";
    parameter Modelica.SIunits.Temperature Tamb = from_degC(35);
    parameter Modelica.SIunits.Temperature Tsky = from_degC(30);
    parameter Modelica.SIunits.Efficiency em_sky = 0.895 "Sky emmisivity";
    parameter Modelica.SIunits.Efficiency em_gr = 0.955 "Ground emmisivity";
    parameter Modelica.SIunits.Temperature Ta = ((em_sky * Tsky ^ 4 + em_gr * Tamb ^ 4) / (em_gr + em_sky)) ^ 0.25;
    parameter Modelica.SIunits.Length dz = 0.42 "Longitude of the pipe segment";
    parameter Modelica.SIunits.MassFlowRate m_dot = 4.2;
    parameter Integer N_tb_pa = 1;
    // ************ Parameters external heat transfer coefficients ***************//
    Modelica.SIunits.CoefficientOfHeatTransfer hc "External coefficient of heat transfer due to forced convection";
    parameter SI.Velocity v_wind = 5 "Wind speed";
    parameter Modelica.SIunits.SpecificHeatCapacity cp_a=1006 "Specific heat capacity of air at ambient temperature";
    parameter Modelica.SIunits.ThermalConductivity k_a=0.026 "Thermal conductivity of air at ambient temperature";
    parameter Modelica.SIunits.Density rho_a=1.184 "Density of air at ambient temperature";
    parameter Modelica.SIunits.DynamicViscosity mu_a=1.845e-5 "Dynamic viscosity at ambient temperature";
    parameter Modelica.SIunits.LinearExpansionCoefficient beta_a=0.003363 "Thermal expansion coefficient at ambient temperature";
    parameter Real Re_a = rho_a*v_wind*D_rcv/mu_a "Reynolds number of air at current wind speed";
    parameter Real Pr_a = cp_a*mu_a/k_a "Prandtl number of air at ambient temperature";
    parameter Modelica.SIunits.KinematicViscosity nu_a = mu_a/rho_a "Kinematic viscosity of air at ambient temperature";
    Real Gr_H "Grashof number for air at ambient temperature";
    Real Nu_nc "Nusselt number for air at ambient temperature";
    Modelica.SIunits.CoefficientOfHeatTransfer h_nc "Coefficient of heat transfer for ambient air due to natural convection";
    parameter Real Nu_fc = if v_wind > 0 then 0.3 + 0.62*Re_a^0.5*Pr_a^0.33/(1+(0.4/Pr_a)^(2/3))^(1/4)*(1+(Re_a/282000)^(5/8))^(4/5) else 0 "Nusselt number due to forced convection";
    parameter Modelica.SIunits.CoefficientOfHeatTransfer h_fc = Nu_fc*k_a/(D_rcv) "Coefficient of heat transfer for ambient air due to forced convection";
    // ************ End parameters external heat transfer coefficients ***************//
  
    Real Bif(start=3) "Internal Biot number";
    Real Nu "Internal Nusselt number";
    Real Re "Reynolds number";
    Real Pr "Prandtl number";
    Modelica.SIunits.Density rho "HTF density";
    Modelica.SIunits.ThermalConductivity kf "HTF thermal conductivity";
    Modelica.SIunits.DynamicViscosity muf "HTF dynamic viscosity";
    Modelica.SIunits.Velocity vf "HTF velocity";
    Modelica.SIunits.SpecificHeatCapacity Cp "HTF Specific heat capacity";
    Modelica.SIunits.ThermalConductivity k "Pipe wall thermal conductivity";
    Real f "Darcy friction factor";
    Real A0(start=500);
    Real A1(start=500);
    Real A2(start=500);
    Real A3(start=500);
    Real A4(start=500);
    Real A5(start=500);
    Real B0(start=500);
    Real g1;
    Real g2;
    Real g3;
    Real g4;
    Real g5;
    Real gp1;
    Real gp2;
    Real gp3;
    Real gp4;
    Real gp5;
  
    Modelica.SIunits.CoefficientOfHeatTransfer hf "Internal coefficient of heat transfer due to forced convection";
    Modelica.SIunits.Temperature Ts;
    Modelica.SIunits.Temperature Ti;
    Modelica.SIunits.Temperature Tin;
    Modelica.SIunits.Temperature Tout;
    Modelica.SIunits.SpecificEnthalpy hin;
    Modelica.SIunits.SpecificEnthalpy hout;
    Modelica.SIunits.SpecificEnthalpy hm;
    Modelica.SIunits.Temperature Tm;
    Modelica.SIunits.HeatFlux q0;
  algorithm
    hc := if v_wind > 0 then (h_nc^3.2 + h_fc^3.2)^(1/3.2) else h_nc;
  
  equation
    k = 0.0163914878 * Ti + 6.8703621459;
    
    // **** Begin external convection  ******//
    Gr_H = Modelica.Constants.g_n*beta_a*(Ts-Tamb)*H_rcv/nu_a^2;
    Nu_nc = 0.098*Pi/2*(Gr_H^(1/3))*((Ts/Tamb)^(-0.14));
    h_nc = Nu_nc*k_a/(H_rcv);
    // **** End external convection  ******//
    
    hout = hin + 2*Pi*k*B0*dz/m_dot;
    hm = hin + Pi*k*B0*dz/m_dot;
    hin = Medium.h_T(Tin);
    hout = Medium.h_T(Tout);
    hm = Medium.h_T(Tm);
    hf = Nu * kf / (2 * Ri) / (1 + Nu * kf / (2 * Ri) * 8.808e-5);
    rho = Medium.rho_T(Tm);
    kf = Medium.lamda_T(Tm);
    muf = Medium.eta_T(Tm);
    vf = m_dot / (N_tb_pa * rho * Pi * Ri ^ 2);
    Cp = Medium.cp_T(Tm);
    Re = max(1, vf * rho * 2 * Ri / muf);
    Pr = max(0, muf * Cp / kf);
    f=(1.82*log10(Re)-1.64)^(-2);
    Nu = (f/8)*(Re-1000)*Pr/(1+12.7*(f/8)^0.5*((Pr^0.66)-1));
    Bif = hf * Ri / k;
    B0 = Bif * (A0 - Tm) / (1 - Bif * log(Ri));
    g1 = g(Ro, 1, Ri, Bif);
    g2 = g(Ro, 2, Ri, Bif);
    g3 = g(Ro, 3, Ri, Bif);
    g4 = g(Ro, 4, Ri, Bif);
    g5 = g(Ro, 5, Ri, Bif);
    gp1 = gp(Ro, 1, Ri, Bif);
    gp2 = gp(Ro, 2, Ri, Bif);
    gp3 = gp(Ro, 3, Ri, Bif);
    gp4 = gp(Ro, 4, Ri, Bif);
    gp5 = gp(Ro, 5, Ri, Bif);
  
  -((ab*q0)/k) + (B0*Pi)/Ro + (hc*Pi*(A0 - Ta + B0*log(Ro)))/k + (1/(8*k))*(em*Pi*sigma*(8*A0^4 + 3*A1^4*g1^4 + 4*A1^3*A3*g1^3*g3 + 12*A1*g1*(A2*A3*g2*g3*(A2*g2 + 2*A4*g4) + A5*(A3^2*g3^2 + A2*g2*(A2*g2 + 2*A4*g4))*g5) + 
       24*A0^2*(A1^2*g1^2 + A2^2*g2^2 + A3^2*g3^2 + A4^2*g4^2 + A5^2*g5^2) + 12*A1^2*g1^2*(A2^2*g2^2 + A3^2*g3^2 + A2*A4*g2*g4 + A4^2*g4^2 + A3*A5*g3*g5 + A5^2*g5^2) + 
       3*(A2^4*g2^4 + A3^4*g3^4 + A4^4*g4^4 + 4*A3*A4^2*A5*g3*g4^2*g5 + 4*A4^2*A5^2*g4^2*g5^2 + A5^4*g5^4 + 4*A2*A3*A4*g2*g3*g4*(A3*g3 + 2*A5*g5) + 4*A3^2*g3^2*(A4^2*g4^2 + A5^2*g5^2) + 
         4*A2^2*g2^2*(A3^2*g3^2 + A4^2*g4^2 + A5^2*g5^2)) + 24*A0*(A1^2*A2*g1^2*g2 + A2*g2*(A2*A4*g2*g4 + 2*A3*A5*g3*g5) + 2*A1*g1*(A2*A3*g2*g3 + A4*g4*(A3*g3 + A5*g5))) - 8*Ta^4 + 
       8*B0*log(Ro)*(4*A0^3 + 3*A1^2*A2*g1^2*g2 + 3*A2*g2*(A2*A4*g2*g4 + 2*A3*A5*g3*g5) + 6*A0*(A1^2*g1^2 + A2^2*g2^2 + A3^2*g3^2 + A4^2*g4^2 + A5^2*g5^2) + 6*A1*g1*(A2*A3*g2*g3 + A4*g4*(A3*g3 + A5*g5)) + 
         B0*log(Ro)*(3*(2*A0^2 + A1^2*g1^2 + A2^2*g2^2 + A3^2*g3^2 + A4^2*g4^2 + A5^2*g5^2) + B0*log(Ro)*(4*A0 + B0*log(Ro)))))) = 0;
  
  (A1*gp1*Pi)/2 + (A1*g1*hc*Pi)/(2*k) - (ab*Pi*q0)/(4*k) + (1/(4*k))*(em*Pi*sigma*(8*A0^3*A1*g1 + 4*A1^3*A2*g1^3*g2 + 3*A1*A2^3*g1*g2^3 + 9*A1^2*A2*A3*g1^2*g2*g3 + 3*A2^3*A3*g2^3*g3 + 6*A1*A2*A3^2*g1*g2*g3^2 + 
       3*A2*A3^3*g2*g3^3 + A1^3*A4*g1^3*g4 + 6*A1*A2^2*A4*g1*g2^2*g4 + 9*A1^2*A3*A4*g1^2*g3*g4 + 6*A2^2*A3*A4*g2^2*g3*g4 + 3*A1*A3^2*A4*g1*g3^2*g4 + 3*A3^3*A4*g3^3*g4 + 6*A1*A2*A4^2*g1*g2*g4^2 + 6*A2*A3*A4^2*g2*g3*g4^2 + 
       3*A3*A4^3*g3*g4^3 + 3*A1^2*A2*A5*g1^2*g2*g5 + A2^3*A5*g2^3*g5 + 12*A1*A2*A3*A5*g1*g2*g3*g5 + 3*A2*A3^2*A5*g2*g3^2*g5 + 9*A1^2*A4*A5*g1^2*g4*g5 + 6*A2^2*A4*A5*g2^2*g4*g5 + 6*A1*A3*A4*A5*g1*g3*g4*g5 + 
       6*A3^2*A4*A5*g3^2*g4*g5 + 3*A2*A4^2*A5*g2*g4^2*g5 + 3*A4^3*A5*g4^3*g5 + 6*A1*A2*A5^2*g1*g2*g5^2 + 6*A2*A3*A5^2*g2*g3*g5^2 + 6*A3*A4*A5^2*g3*g4*g5^2 + 3*A4*A5^3*g4*g5^3 + 
       12*A0^2*(A1*A2*g1*g2 + A2*A3*g2*g3 + A4*g4*(A3*g3 + A5*g5)) + 6*A0*(A1^3*g1^3 + A1^2*A3*g1^2*g3 + A3^2*A5*g3^2*g5 + A2^2*g2^2*(A3*g3 + A5*g5) + 2*A2*A4*g2*g4*(A3*g3 + A5*g5) + 
         2*A1*g1*(A2^2*g2^2 + A3^2*g3^2 + A2*A4*g2*g4 + A4^2*g4^2 + A3*A5*g3*g5 + A5^2*g5^2)) + 6*B0*(4*A0^2*A1*g1 + A1^3*g1^3 + A1^2*A3*g1^2*g3 + A2^2*A3*g2^2*g3 + 2*A2*A3*A4*g2*g3*g4 + A2^2*A5*g2^2*g5 + A3^2*A5*g3^2*g5 + 
         2*A2*A4*A5*g2*g4*g5 + 4*A0*(A1*A2*g1*g2 + A2*A3*g2*g3 + A3*A4*g3*g4 + A4*A5*g4*g5) + 2*A1*g1*(A2^2*g2^2 + A3^2*g3^2 + A2*A4*g2*g4 + A4^2*g4^2 + A3*A5*g3*g5 + A5^2*g5^2))*log(Ro) + 
       12*B0^2*(2*A0*A1*g1 + A1*A2*g1*g2 + A2*A3*g2*g3 + A3*A4*g3*g4 + A4*A5*g4*g5)*log(Ro)^2 + 8*A1*B0^3*g1*log(Ro)^3)) = 0;
  
  (A2*gp2*Pi)/2 + (A2*g2*hc*Pi)/(2*k) - (ab*q0)/(3*k) + (1/(8*k))*(em*Pi*sigma*(2*A1^4*g1^4 + 16*A0^3*A2*g2 + 9*A1^2*A2^2*g1^2*g2^2 + 6*A1^3*A3*g1^3*g3 + 18*A1*A2^2*A3*g1*g2^2*g3 + 6*A1^2*A3^2*g1^2*g3^2 + 
       3*A2^2*A3^2*g2^2*g3^2 + 6*A1*A3^3*g1*g3^3 + 12*A1^2*A2*A4*g1^2*g2*g4 + 8*A2^3*A4*g2^3*g4 + 24*A1*A2*A3*A4*g1*g2*g3*g4 + 12*A2*A3^2*A4*g2*g3^2*g4 + 6*A1^2*A4^2*g1^2*g4^2 + 12*A1*A3*A4^2*g1*g3*g4^2 + 
       3*A3^2*A4^2*g3^2*g4^2 + 6*A2*A4^3*g2*g4^3 + 2*A1^3*A5*g1^3*g5 + 6*A1*A2^2*A5*g1*g2^2*g5 + 12*A1^2*A3*A5*g1^2*g3*g5 + 18*A2^2*A3*A5*g2^2*g3*g5 + 6*A1*A3^2*A5*g1*g3^2*g5 + 6*A3^3*A5*g3^3*g5 + 24*A1*A2*A4*A5*g1*g2*g4*g5 + 
       12*A2*A3*A4*A5*g2*g3*g4*g5 + 6*A1*A4^2*A5*g1*g4^2*g5 + 12*A3*A4^2*A5*g3*g4^2*g5 + 6*A1^2*A5^2*g1^2*g5^2 + 12*A1*A3*A5^2*g1*g3*g5^2 + 12*A2*A4*A5^2*g2*g4*g5^2 + 3*A4^2*A5^2*g4^2*g5^2 + 6*A3*A5^3*g3*g5^3 + 
       12*A0^2*(A1^2*g1^2 + 2*A1*A3*g1*g3 + 2*A2*A4*g2*g4 + 2*A3*A5*g3*g5) + 12*A0*(A2^3*g2^3 + A1^2*g1^2*(2*A2*g2 + A4*g4) + 2*A1*g1*(A2*g2 + A4*g4)*(A3*g3 + A5*g5) + A3*A4*g3*g4*(A3*g3 + 2*A5*g5) + 
         2*A2*g2*(A3^2*g3^2 + A4^2*g4^2 + A5^2*g5^2)) + 12*B0*(4*A0^2*A2*g2 + A2^3*g2^3 + 2*A2*A3^2*g2*g3^2 + A3^2*A4*g3^2*g4 + 2*A2*A4^2*g2*g4^2 + A1^2*g1^2*(2*A2*g2 + A4*g4) + 2*A3*A4*A5*g3*g4*g5 + 2*A2*A5^2*g2*g5^2 + 
         2*A1*g1*(A2*g2 + A4*g4)*(A3*g3 + A5*g5) + 2*A0*(A1^2*g1^2 + 2*A1*A3*g1*g3 + 2*A2*A4*g2*g4 + 2*A3*A5*g3*g5))*log(Ro) + 12*B0^2*(A1^2*g1^2 + 4*A0*A2*g2 + 2*A1*A3*g1*g3 + 2*A2*A4*g2*g4 + 2*A3*A5*g3*g5)*log(Ro)^2 + 
       16*A2*B0^3*g2*log(Ro)^3)) = 0;
  
  (A3*gp3*Pi)/2 + (A3*g3*hc*Pi)/(2*k) + (1/(4*k))*(em*Pi*sigma*(3*A1^3*A2*g1^3*g2 + 3*A1*A2^3*g1*g2^3 + 8*A0^3*A3*g3 + 6*A1^2*A2*A3*g1^2*g2*g3 + A2^3*A3*g2^3*g3 + 9*A1*A2*A3^2*g1*g2*g3^2 + 3*A1^3*A4*g1^3*g4 + 
       6*A1*A2^2*A4*g1*g2^2*g4 + 3*A1^2*A3*A4*g1^2*g3*g4 + 6*A2^2*A3*A4*g2^2*g3*g4 + 9*A1*A3^2*A4*g1*g3^2*g4 + 6*A1*A2*A4^2*g1*g2*g4^2 + 3*A2*A3*A4^2*g2*g3*g4^2 + 3*A1*A4^3*g1*g4^3 + 6*A1^2*A2*A5*g1^2*g2*g5 + 
       3*A2^3*A5*g2^3*g5 + 6*A1*A2*A3*A5*g1*g2*g3*g5 + 9*A2*A3^2*A5*g2*g3^2*g5 + 3*A1^2*A4*A5*g1^2*g4*g5 + 3*A2^2*A4*A5*g2^2*g4*g5 + 12*A1*A3*A4*A5*g1*g3*g4*g5 + 3*A3^2*A4*A5*g3^2*g4*g5 + 6*A2*A4^2*A5*g2*g4^2*g5 + 
       6*A1*A2*A5^2*g1*g2*g5^2 + 6*A1*A4*A5^2*g1*g4*g5^2 + 3*A3*A4*A5^2*g3*g4*g5^2 + 3*A2*A5^3*g2*g5^3 + 12*A0^2*(A1*g1*(A2*g2 + A4*g4) + A2*A5*g2*g5) + 
       2*A0*(A1^3*g1^3 + 6*A2^2*A3*g2^2*g3 + 3*A3^3*g3^3 + 6*A3*A4^2*g3*g4^2 + 3*A4^2*A5*g4^2*g5 + 6*A3*A5^2*g3*g5^2 + 6*A2*A4*g2*g4*(A3*g3 + A5*g5) + 3*A1^2*g1^2*(2*A3*g3 + A5*g5) + 
         3*A1*g1*(A2^2*g2^2 + 2*A2*A4*g2*g4 + 2*A3*A5*g3*g5)) + 2*B0*(A1^3*g1^3 + 3*A1^2*g1^2*(2*A3*g3 + A5*g5) + 3*A1*g1*(A2^2*g2^2 + 2*A2*A4*g2*g4 + 4*A0*(A2*g2 + A4*g4) + 2*A3*A5*g3*g5) + 
         3*(4*A0^2*A3*g3 + 2*A2^2*A3*g2^2*g3 + A3^3*g3^3 + 2*A3*A4^2*g3*g4^2 + 4*A0*A2*A5*g2*g5 + A4^2*A5*g4^2*g5 + 2*A3*A5^2*g3*g5^2 + 2*A2*A4*g2*g4*(A3*g3 + A5*g5)))*log(Ro) + 
       12*B0^2*(2*A0*A3*g3 + A1*g1*(A2*g2 + A4*g4) + A2*A5*g2*g5)*log(Ro)^2 + 8*A3*B0^3*g3*log(Ro)^3)) = 0;
  
  (A4*gp4*Pi)/2 + (A4*g4*hc*Pi)/(2*k) + (ab*q0)/(15*k) + (1/(16*k))*(em*Pi*sigma*(A1^4*g1^4 + 12*A1^3*g1^3*(A3*g3 + A5*g5) + 6*A1^2*g1^2*(2*A2^2*g2^2 + 4*A2*A4*g2*g4 + 4*A0*(A2*g2 + 2*A4*g4) + A3*g3*(A3*g3 + 2*A5*g5)) + 
       12*A1*g1*(4*A0^2*(A3*g3 + A5*g5) + 4*A0*A2*g2*(A3*g3 + A5*g5) + 2*A2^2*g2^2*(A3*g3 + A5*g5) + 2*A2*A4*g2*g4*(2*A3*g3 + A5*g5) + (A3*g3 + A5*g5)*(A3^2*g3^2 + 3*A4^2*g4^2 + A3*A5*g3*g5 + A5^2*g5^2)) + 
       2*(12*A0^2*A2^2*g2^2 + 2*A2^4*g2^4 + 16*A0^3*A4*g4 + A3^2*A5*g3^2*g5*(2*A3*g3 + 3*A5*g5) + 6*A2*A4*g2*g4*(A3^2*g3^2 + 4*A3*A5*g3*g5 + A5^2*g5^2) + 3*A2^2*g2^2*(2*A3^2*g3^2 + 3*A4^2*g4^2 + 2*A3*A5*g3*g5 + 2*A5^2*g5^2) + 
         12*A0*(2*A2^2*A4*g2^2*g4 + A2*A3*g2*g3*(A3*g3 + 2*A5*g5) + A4*g4*(2*A3^2*g3^2 + A4^2*g4^2 + 2*A3*A5*g3*g5 + 2*A5^2*g5^2))) + 
       24*B0*(2*A0*A2^2*g2^2 + A2*A3^2*g2*g3^2 + 4*A0^2*A4*g4 + 2*A2^2*A4*g2^2*g4 + 2*A3^2*A4*g3^2*g4 + A4^3*g4^3 + A1^2*g1^2*(A2*g2 + 2*A4*g4) + 2*A2*A3*A5*g2*g3*g5 + 2*A3*A4*A5*g3*g4*g5 + 2*A4*A5^2*g4*g5^2 + 
         2*A1*g1*(2*A0 + A2*g2)*(A3*g3 + A5*g5))*log(Ro) + 24*B0^2*(A2^2*g2^2 + 4*A0*A4*g4 + 2*A1*g1*(A3*g3 + A5*g5))*log(Ro)^2 + 32*A4*B0^3*g4*log(Ro)^3)) = 0;
  
  (A5*gp5*Pi)/2 + (A5*g5*hc*Pi)/(2*k) + (1/(4*k))*(em*Pi*sigma*(12*A0^2*A2*A3*g2*g3 + 3*A2^3*A3*g2^3*g3 + 3*A2*A3^3*g2*g3^3 + 12*A0*A2*A3*A4*g2*g3*g4 + 3*A2^2*A3*A4*g2^2*g3*g4 + A3^3*A4*g3^3*g4 + 6*A0*A3*A4^2*g3*g4^2 + 
       6*A2*A3*A4^2*g2*g3*g4^2 + A1^3*g1^3*(A2*g2 + 3*A4*g4) + 8*A0^3*A5*g5 + 12*A0*A2^2*A5*g2^2*g5 + 12*A0*A3^2*A5*g3^2*g5 + 6*A2^2*A4*A5*g2^2*g4*g5 + 3*A3^2*A4*A5*g3^2*g4*g5 + 12*A0*A4^2*A5*g4^2*g5 + 
       3*A2*A4^2*A5*g2*g4^2*g5 + 9*A2*A3*A5^2*g2*g3*g5^2 + 6*A0*A5^3*g5^3 + 3*A1^2*g1^2*(A3*A4*g3*g4 + 2*A2*g2*(A3*g3 + A5*g5) + 2*A0*(A3*g3 + 2*A5*g5)) + 
       A1*g1*(A2^3*g2^3 + 12*A0^2*A4*g4 + 6*A2^2*A4*g2^2*g4 + 6*A0*(A2^2*g2^2 + A3^2*g3^2 + 2*A2*A4*g2*g4) + 3*A2*g2*(A3^2*g3^2 + A4^2*g4^2 + 4*A3*A5*g3*g5) + 3*A4*g4*(2*A3^2*g3^2 + A4^2*g4^2 + 4*A3*A5*g3*g5 + 3*A5^2*g5^2)) + 
       6*B0*(4*A0*A2*A3*g2*g3 + 2*A2*A3*A4*g2*g3*g4 + A3*A4^2*g3*g4^2 + A1*g1*(A2^2*g2^2 + A3^2*g3^2 + 4*A0*A4*g4 + 2*A2*A4*g2*g4) + 4*A0^2*A5*g5 + 2*A2^2*A5*g2^2*g5 + 2*A3^2*A5*g3^2*g5 + 2*A4^2*A5*g4^2*g5 + A5^3*g5^3 + 
         A1^2*g1^2*(A3*g3 + 2*A5*g5))*log(Ro) + 12*B0^2*(A2*A3*g2*g3 + A1*A4*g1*g4 + 2*A0*A5*g5)*log(Ro)^2 + 8*A5*B0^3*g5*log(Ro)^3)) = 0;
  
    Ts = A0 + B0 * log(Ro) + A1 * g1 + A2 * g2  + A3 * g3 + A4 * g4 + A5 * g5;
    Ti = A0 + B0 * log(Ri) + A1 * g(Ri, 1, Ri, Bif) + A2 * g(Ri, 2, Ri, Bif) + A3 * g(Ri, 3, Ri, Bif) + A4 * g(Ri, 4, Ri, Bif) + A5 * g(Ri, 5, Ri, Bif);
  end FiveCoefficients;

  model SectionFiniteDifference
  replaceable package Medium = SolarTherm.Media.MoltenSalt.MoltenSalt_utilities;
  // ****************  CONSTANTS *********************
  parameter Real pi = Modelica.Constants.pi;
  parameter Real sigma = Modelica.Constants.sigma;
  parameter Real g = Modelica.Constants.g_n;
  // ****************  COATING PROPERTIES *********************
  parameter Real ab = 0.93;
  parameter Real em = 0.87;
  // ****************  WEATHER *********************
  parameter Modelica.SIunits.Temperature Tamb = 298.15;
  parameter Modelica.SIunits.Temperature Tsky = 303.15;
  parameter Real em_sky = 0.895;
  parameter Real em_gr = 0.955;
  parameter Modelica.SIunits.Temperature Ta = ((em_gr*Tamb^4 + em_sky*Tsky^4)/(em_gr + em_sky))^0.25;
  // ****************  NUMBER OF NODES *********************
  parameter Integer m = 10 "Number of nodes in phi";
  parameter Integer n = 8 "Number of nodes in radial";
  parameter Modelica.SIunits.Temperature T_f = Modelica.SIunits.Conversions.from_degC(290);
  // ****************  RECEIVER GEOMETRY *********************
  parameter Modelica.SIunits.Height H_rcv = 10.5 "Receiver height"; //Gemasolar solar power tower https://doi.org/10.1016/j.solener.2015.12.055
  parameter Modelica.SIunits.Diameter D_rcv = 8.5 "Receiver diameter"; //Gemasolar solar power tower https://doi.org/10.1016/j.solener.2015.12.055
  parameter Modelica.SIunits.Radius Ri = 21e-3 "Inner tube radius";
  parameter Modelica.SIunits.Radius Ro = 22.5e-3 "Outer tube radius";
  parameter Modelica.SIunits.Length dr = (Ro-Ri)/n;
  parameter Modelica.SIunits.Angle dth = pi/m;
  parameter Modelica.SIunits.Radius r[n] = linspace(Ri,Ro,n);
  parameter Modelica.SIunits.MassFlowRate m_dot = 4.2;
  parameter Integer N_tb_pa = 1;
  // **************** TUBE METAL PROPERTIES *********************
  parameter Modelica.SIunits.ThermalConductivity k = 20;
  parameter Modelica.SIunits.ThermalDiffusivity alpha = k/8440/513; // Inconel 625 density (kg/m3)
  // ************ AMBIENT AIRE PROPERTIES ***************//
  parameter Modelica.SIunits.Velocity v_wind = 0 "Wind speed";
  parameter Modelica.SIunits.SpecificHeatCapacity cp_a=1006 "Specific heat capacity of air at ambient temperature";
  parameter Modelica.SIunits.ThermalConductivity k_a=0.026 "Thermal conductivity of air at ambient temperature";
  parameter Modelica.SIunits.Density rho_a=1.184 "Density of air at ambient temperature";
  parameter Modelica.SIunits.DynamicViscosity mu_a=1.845e-5 "Dynamic viscosity at ambient temperature";
  parameter Modelica.SIunits.LinearExpansionCoefficient beta_a=0.003363 "Thermal expansion coefficient at ambient temperature";
  // ************ FORCED CONVECTION PARAMETERS ***************//
  parameter Modelica.SIunits.KinematicViscosity nu_a = mu_a/rho_a "Kinematic viscosity of air at ambient temperature";
  parameter Real Re_a = rho_a*v_wind*D_rcv/mu_a "Reynolds number of air at current wind speed";
  parameter Real Pr_a = cp_a*mu_a/k_a "Prandtl number of air at ambient temperature";
  parameter Real Nu_fc = if v_wind > 0 then 0.3 + 0.62*Re_a^0.5*Pr_a^0.33/(1+(0.4/Pr_a)^(2/3))^(1/4)*(1+(Re_a/282000)^(5/8))^(4/5) else 0 "Nusselt number due to forced convection";
  parameter Modelica.SIunits.CoefficientOfHeatTransfer h_fc = Nu_fc*k_a/(D_rcv) "Coefficient of heat transfer for ambient air due to forced convection";
  // ************ TUBE TEMPERATURE FIELD ***************//
  Modelica.SIunits.Temperature T[n,m] "Temperature field";
  // ************ INCOMING HEAT FLUX ***************//
  Modelica.SIunits.HeatFlux q_r[m];
  // ************ NATURAL CONVECTION VARIABLES ***************//
  Real Gr_H "Grashof number for air at ambient temperature";
  Real Nu_nc "Nusselt number for air at ambient temperature";
  Modelica.SIunits.Temperature T_av_crow;
  Modelica.SIunits.CoefficientOfHeatTransfer h_nc "Coefficient of heat transfer for ambient air due to natural convection";
  Modelica.SIunits.CoefficientOfHeatTransfer h_e "External coefficient of heat transfer due to forced convection";
  // ************ HTF PROPERTIES ***************//
  Modelica.SIunits.Density rho "HTF density";
  Modelica.SIunits.DynamicViscosity muf "HTF dynamic viscosity";
  Modelica.SIunits.ThermalConductivity kf "HTF thermal conductivity";
  Modelica.SIunits.SpecificHeatCapacity Cp "HTF Specific heat capacity";
  // ************ HTF FORCED CONVECTION ***************//
  Real Nu "Internal Nusselt number";
  Real Re "Reynolds number";
  Real Pr "Prandtl number";
  Real f "Darcy friction factor";
  Modelica.SIunits.Velocity vf "HTF velocity";
  Modelica.SIunits.CoefficientOfHeatTransfer h_f;
  
  initial equation
  	T = T_f*ones(n,m);
  
  equation
  
  T_av_crow = sum(T[end,j] for j in 1:m)/m;
  Gr_H = g*beta_a*(T_av_crow-Tamb)*H_rcv/nu_a^2;
  Nu_nc = 0.098*pi/2*(Gr_H^(1/3))*((T_av_crow/Tamb)^(-0.14));
  h_nc = Nu_nc*k_a/(H_rcv);
  h_e = if v_wind > 0 then (h_nc^3.2 + h_fc^3.2)^(1/3.2) else h_nc;
  
  h_f = Nu * kf / (2 * Ri) / (1 + Nu * kf / (2 * Ri) * 8.808e-5);
  rho = Medium.rho_T(T_f);
  kf = Medium.lamda_T(T_f);
  muf = Medium.eta_T(T_f);
  vf = m_dot / (N_tb_pa * rho * pi * Ri ^ 2);
  Cp = Medium.cp_T(T_f);
  Re = max(1, vf * rho * 2 * Ri / muf);
  Pr = max(0, muf * Cp / kf);
  f = (1.82*log10(Re)-1.64)^(-2);
  Nu = (f/8)*(Re-1000)*Pr/(1+12.7*(f/8)^0.5*((Pr^0.66)-1));
  
  for j in 1:m loop
  	q_r[j] = if (j-1)*dth > pi/2 then 0 else 0.2e6*cos((j-1)*dth);
  end for;
  
  for i in 1:n loop
  	for j in 1:m loop
  		if i == 1 then
  			if j == 1 then
  				(2*T[i+1,j] - 2*T[i,j])/dr^2 + 2*h_f/k/dr*(T_f - T[i,j]) + 1/r[i]*h_f/k*(T[i,j] - T_f) + 1/r[i]^2*(2*T[i,j+1] - 2*T[i,j])/dth^2 = 1/alpha*der(T[i,j]);
  			elseif j == m then
  				(2*T[i+1,j] - 2*T[i,j])/dr^2 + 2*h_f/k/dr*(T_f - T[i,j]) + 1/r[i]*h_f/k*(T[i,j] - T_f) + 1/r[i]^2*(2*T[i,j-1] - 2*T[i,j])/dth^2 = 1/alpha*der(T[i,j]);
  			else
  				(2*T[i+1,j] - 2*T[i,j])/dr^2 + 2*h_f/k/dr*(T_f - T[i,j]) + 1/r[i]*h_f/k*(T[i,j] - T_f) + 1/r[i]^2*(T[i,j+1] - 2*T[i,j] + T[i,j-1])/dth^2 = 1/alpha*der(T[i,j]);
  			end if;
  		elseif i == n then
  			if j == 1 then
  				(2*T[i-1,j] - 2*T[i,j])/dr^2 + 2/k/dr*(q_r[j] + h_e*(Tamb - T[i,j]) + sigma*em*(Ta^4 - T[i,j]^4)) + 1/r[i]*1/k*(q_r[j] + h_e*(Tamb - T[i,j]) + sigma*em*(Ta^4 - T[i,j]^4)) + 1/r[i]^2*(2*T[i,j+1] - 2*T[i,j])/dth^2 = 1/alpha*der(T[i,j]);
  			elseif j == m then
  				(2*T[i-1,j] - 2*T[i,j])/dr^2 + 2/k/dr*(q_r[j] + h_e*(Tamb - T[i,j]) + sigma*em*(Ta^4 - T[i,j]^4)) + 1/r[i]*1/k*(q_r[j] + h_e*(Tamb - T[i,j]) + sigma*em*(Ta^4 - T[i,j]^4)) + 1/r[i]^2*(2*T[i,j-1] - 2*T[i,j])/dth^2 = 1/alpha*der(T[i,j]);
  			else
  				(2*T[i-1,j] - 2*T[i,j])/dr^2 + 2/k/dr*(q_r[j] + h_e*(Tamb - T[i,j]) + sigma*em*(Ta^4 - T[i,j]^4)) + 1/r[i]*1/k*(q_r[j] + h_e*(Tamb - T[i,j]) + sigma*em*(Ta^4 - T[i,j]^4)) + 1/r[i]^2*(T[i,j+1] - 2*T[i,j] + T[i,j-1])/dth^2 = 1/alpha*der(T[i,j]);
  			end if;
  		else
  			if j == 1 then
  				(T[i+1,j] - 2*T[i,j] + T[i-1,j])/dr^2 + 1/r[i]*(T[i+1,j] - T[i-1,j])/2/dr + 1/r[i]^2*(2*T[i,j+1] - 2*T[i,j])/dth^2 = 1/alpha*der(T[i,j]);
  			elseif j == m then
  				(T[i+1,j] - 2*T[i,j] + T[i-1,j])/dr^2 + 1/r[i]*(T[i+1,j] - T[i-1,j])/2/dr + 1/r[i]^2*(2*T[i,j-1] - 2*T[i,j])/dth^2 = 1/alpha*der(T[i,j]);
  			else
  				(T[i+1,j] - 2*T[i,j] + T[i-1,j])/dr^2 + 1/r[i]*(T[i+1,j] - T[i-1,j])/2/dr + 1/r[i]^2*(T[i,j+1] - 2*T[i,j] + T[i,j-1])/dth^2 = 1/alpha*der(T[i,j]);
  			end if;
  		end if;				
  	end for;
  end for;
  end SectionFiniteDifference;
  
  model TwoCoefficientsCombinedCoefficient
  import Modelica.Math.*;
    import Modelica.SIunits.Conversions.*;
    replaceable package Medium = SolarTherm.Media.MoltenSalt.MoltenSalt_utilities;
  
    parameter SI.Height H_rcv = 10.5 "Receiver height"; //Gemasolar solar power tower https://doi.org/10.1016/j.solener.2015.12.055
    parameter SI.Diameter D_rcv = 8.5 "Receiver diameter"; //Gemasolar solar power tower https://doi.org/10.1016/j.solener.2015.12.055
    parameter Modelica.SIunits.Radius Ri = 21e-3 "Inner tube radius";
    parameter Modelica.SIunits.Radius Ro = 22.5e-3 "Outer tube radius";
    parameter Real Pi = Modelica.Constants.pi;
    parameter Real sigma = Modelica.Constants.sigma "Steffan-Boltzmann constant";
    parameter Modelica.SIunits.Efficiency ab = 0.93 "Coating absorptance";
    parameter Modelica.SIunits.Efficiency em = 0.87 "Coating emmisivity";
    parameter Modelica.SIunits.Temperature Tamb = from_degC(35);
    parameter Modelica.SIunits.Temperature Tsky = from_degC(30);
    parameter Modelica.SIunits.Efficiency em_sky = 0.895 "Sky emmisivity";
    parameter Modelica.SIunits.Efficiency em_gr = 0.955 "Ground emmisivity";
    parameter Modelica.SIunits.Temperature Ta = ((em_sky * Tsky ^ 4 + em_gr * Tamb ^ 4) / (em_gr + em_sky)) ^ 0.25;
    parameter Modelica.SIunits.Length dz = 0.42 "Longitude of the pipe segment";
    parameter Modelica.SIunits.MassFlowRate m_dot = 4.2;
    parameter Integer N_tb_pa = 1;
    // ************ Parameters external heat transfer coefficients ***************//
    Modelica.SIunits.CoefficientOfHeatTransfer hc "External coefficient of heat transfer due to forced convection";
    parameter SI.Velocity v_wind = 5 "Wind speed";
    parameter Modelica.SIunits.SpecificHeatCapacity cp_a=1006 "Specific heat capacity of air at ambient temperature";
    parameter Modelica.SIunits.ThermalConductivity k_a=0.026 "Thermal conductivity of air at ambient temperature";
    parameter Modelica.SIunits.Density rho_a=1.184 "Density of air at ambient temperature";
    parameter Modelica.SIunits.DynamicViscosity mu_a=1.845e-5 "Dynamic viscosity at ambient temperature";
    parameter Modelica.SIunits.LinearExpansionCoefficient beta_a=0.003363 "Thermal expansion coefficient at ambient temperature";
    parameter Real Re_a = rho_a*v_wind*D_rcv/mu_a "Reynolds number of air at current wind speed";
    parameter Real Pr_a = cp_a*mu_a/k_a "Prandtl number of air at ambient temperature";
    parameter Modelica.SIunits.KinematicViscosity nu_a = mu_a/rho_a "Kinematic viscosity of air at ambient temperature";
    Real Gr_H "Grashof number for air at ambient temperature";
    Real Nu_nc "Nusselt number for air at ambient temperature";
    Modelica.SIunits.CoefficientOfHeatTransfer h_nc "Coefficient of heat transfer for ambient air due to natural convection";
    parameter Real Nu_fc = if v_wind > 0 then 0.3 + 0.62*Re_a^0.5*Pr_a^0.33/(1+(0.4/Pr_a)^(2/3))^(1/4)*(1+(Re_a/282000)^(5/8))^(4/5) else 0 "Nusselt number due to forced convection";
    parameter Modelica.SIunits.CoefficientOfHeatTransfer h_fc = Nu_fc*k_a/(D_rcv) "Coefficient of heat transfer for ambient air due to forced convection";
    // ************ End parameters external heat transfer coefficients ***************//
  
    Real Bif(start=3) "Internal Biot number";
    Real Nu "Internal Nusselt number";
    Real Re "Reynolds number";
    Real Pr "Prandtl number";
    Modelica.SIunits.Density rho "HTF density";
    Modelica.SIunits.ThermalConductivity kf "HTF thermal conductivity";
    Modelica.SIunits.DynamicViscosity muf "HTF dynamic viscosity";
    Modelica.SIunits.Velocity vf "HTF velocity";
    Modelica.SIunits.SpecificHeatCapacity Cp "HTF Specific heat capacity";
    Modelica.SIunits.ThermalConductivity k "Pipe wall thermal conductivity";
    Real f "Darcy friction factor";
    Real A0(start=500);
    Real A1(start=500);
    Real A2(start=500);
    Real A3(start=500);
    Real A4(start=500);
    Real A5(start=500);
    Real B0(start=500);
    Real g1;
    Real g2;
    Real g3;
    Real g4;
    Real g5;
    Real gp1;
    Real gp2;
    Real gp3;
    Real gp4;
    Real gp5;
  
    Modelica.SIunits.CoefficientOfHeatTransfer hf "Internal coefficient of heat transfer due to forced convection";
    Modelica.SIunits.Temperature Ts;
    Modelica.SIunits.Temperature Ti;
    Modelica.SIunits.Temperature Tin;
    Modelica.SIunits.Temperature Tout;
    Modelica.SIunits.SpecificEnthalpy hin;
    Modelica.SIunits.SpecificEnthalpy hout;
    Modelica.SIunits.SpecificEnthalpy hm;
    Modelica.SIunits.Temperature Tm;
    Modelica.SIunits.HeatFlux q0;
  algorithm
    hc := if v_wind > 0 then (h_nc^3.2 + h_fc^3.2)^(1/3.2) else h_nc;
  
  equation
    k = 0.0163914878 * Ti + 6.8703621459;
    
    // **** Begin external convection  ******//
    Gr_H = Modelica.Constants.g_n*beta_a*(Ts-Tamb)*H_rcv/nu_a^2;
    Nu_nc = 0.098*Pi/2*(Gr_H^(1/3))*((Ts/Tamb)^(-0.14));
    h_nc = Nu_nc*k_a/(H_rcv);
    // **** End external convection  ******//
    
    hout = hin + 2*Pi*k*B0*dz/m_dot;
    hm = hin + Pi*k*B0*dz/m_dot;
    hin = Medium.h_T(Tin);
    hout = Medium.h_T(Tout);
    hm = Medium.h_T(Tm);
    hf = Nu * kf / (2 * Ri) / (1 + Nu * kf / (2 * Ri) * 8.808e-5);
    rho = Medium.rho_T(Tm);
    kf = Medium.lamda_T(Tm);
    muf = Medium.eta_T(Tm);
    vf = m_dot / (N_tb_pa * rho * Pi * Ri ^ 2);
    Cp = Medium.cp_T(Tm);
    Re = max(1, vf * rho * 2 * Ri / muf);
    Pr = max(0, muf * Cp / kf);
    f=(1.82*log10(Re)-1.64)^(-2);
    Nu = (f/8)*(Re-1000)*Pr/(1+12.7*(f/8)^0.5*((Pr^0.66)-1));
    Bif = hf * Ri / k;
    B0 = Bif * (A0 - Tm) / (1 - Bif * log(Ri));
    g1 = g(Ro, 1, Ri, Bif);
    g2 = g(Ro, 2, Ri, Bif);
    g3 = g(Ro, 3, Ri, Bif);
    g4 = g(Ro, 4, Ri, Bif);
    g5 = g(Ro, 5, Ri, Bif);
    gp1 = gp(Ro, 1, Ri, Bif);
    gp2 = gp(Ro, 2, Ri, Bif);
    gp3 = gp(Ro, 3, Ri, Bif);
    gp4 = gp(Ro, 4, Ri, Bif);
    gp5 = gp(Ro, 5, Ri, Bif);
  
  -((ab*q0)/k) + (B0*Pi)/Ro + (hc*Pi*(A0 - Ta + B0*Log[Ro]))/k = 0;
  (A1*gp1*Pi)/2 + (A1*g1*hc*Pi)/(2*k) - (ab*Pi*q0)/(4*k) = 0;
  (A2*gp2*Pi)/2 + (A2*g2*hc*Pi)/(2*k) - (ab*q0)/(3*k) = 0;
  (A3*gp3*Pi)/2 + (A3*g3*hc*Pi)/(2*k) = 0;
  (A4*gp4*Pi)/2 + (A4*g4*hc*Pi)/(2*k) + (ab*q0)/(15*k) = 0;
  (A5*gp5*Pi)/2 + (A5*g5*hc*Pi)/(2*k) = 0;
  
    Ts = A0 + B0 * log(Ro) + A1 * g1 + A2 * g2 + A3 * g3 + A4 * g4 + A5 * g5;
    Ti = A0 + B0 * log(Ri) + A1 * g(Ri, 1, Ri, Bif) + A2 * g(Ri, 2, Ri, Bif) + A3 * g(Ri, 3, Ri, Bif) + A4 * g(Ri, 4, Ri, Bif) + A5 * g(Ri, 5, Ri, Bif);
  end TwoCoefficientsCombinedCoefficient;
end PipeSection;