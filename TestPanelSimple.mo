within receiver;
model TestPanelSimple
  extends Modelica.Icons.Example;
  import SI = Modelica.SIunits;
  import Modelica.SIunits.Conversions.*;
  replaceable package Medium = SolarTherm.Media.MoltenSalt.MoltenSalt_ph;
  parameter SI.Temperature T_in_des = from_degC(290) "Receiver inlet temperature";
  parameter SI.Temperature T_out_des = from_degC(565) "Receiver inlet temperature";
  parameter SI.Efficiency eff_blk = 0.51 "Power block efficiency at design point";
  parameter SI.Power P_name = 100e6 "Nameplate rating of power block";
  parameter Real SM = 2.7 "Solar multiple";
  parameter SI.Power P_gross = P_name * 111 / 100 "Power block gross rating at design point";
  parameter SI.HeatFlowRate Q_flow_ref_blk = P_gross / eff_blk "Thermal power to power block at design";
  parameter Medium.ThermodynamicState state_in_des = Medium.setState_pTX(p_blk, T_in_des) "medium state: pressure,temperature,composition";
  parameter Medium.ThermodynamicState state_out_des = Medium.setState_pTX(p_blk, T_out_des) "medium state: pressure,temperature,composition";
  parameter SI.SpecificEnthalpy h_in_des = Medium.specificEnthalpy(state_in_des) "specific inlet enthalpy at design point";
  parameter SI.SpecificEnthalpy h_out_des = Medium.specificEnthalpy(state_out_des) "specific outlet enthalpy at design point";
  parameter SI.MassFlowRate m_flow_blk = Q_flow_ref_blk * SM / (h_out_des - h_in_des) "Mass flow rate to power block at design";
  parameter SI.Pressure p_blk = 1e5 "Receiver operation pressure";
  // sink
  Modelica.Fluid.Sources.MassFlowSource_T sink(redeclare package Medium = SolarTherm.Media.MoltenSalt.MoltenSalt_ph, T = T_in_des, m_flow = 137.33, nPorts = 1) annotation(
    Placement(visible = true, transformation(origin = {-12, -30}, extent = {{10, -10}, {-10, 10}}, rotation = -90)));
  // source
  Modelica.Fluid.Sources.FixedBoundary source(redeclare package Medium = SolarTherm.Media.MoltenSalt.MoltenSalt_ph, nPorts = 1, p = p_blk, use_p = true) annotation(
    Placement(visible = true, transformation(origin = {-12, 56}, extent = {{10, -10}, {-10, 10}}, rotation = 90)));
  // Flow paths
  Receiver.PanelSimple flowpath1(H_rcv = 10.5,N_start = 0, N_tb_pa = 31, Nel = 25, Npa_fl = 9, a = 0.021, b = 0.0225, w_pa = 1.5) annotation(
    Placement(visible = true, transformation(origin = {-12, 12}, extent = {{-13, -13}, {13, 13}}, rotation = 90)));

  equation
  connect(sink.ports[1], flowpath1.fluid_a) annotation(
    Line(points = {{-12, -20}, {-12, 2}}, color = {0, 127, 255}));
  connect(flowpath1.fluid_b, source.ports[1]) annotation(
    Line(points = {{-12, 22}, {-12, 22}, {-12, 46}, {-12, 46}}, color = {0, 127, 255}));
  annotation(
    uses(Modelica(version = "3.2.2")),
    experiment(StartTime = 0, StopTime = 200, Tolerance = 1e-6, Interval = 0.4),
    Diagram(coordinateSystem(extent = {{-100, -100}, {100, 100}})),
    Icon(coordinateSystem(extent = {{-100, -100}, {100, 100}})),
    version = "");
end TestPanelSimple;
