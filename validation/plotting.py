# -*- coding: utf-8 -*-
import matplotlib as mpl
mpl.rcParams["figure.dpi"] = 100.
mpl.rcParams["figure.figsize"] = [11, 5]
mpl.rcParams['font.size'] = 12
import matplotlib.pyplot as plt
import matplotlib.font_manager as font_manager
prop = font_manager.FontProperties(fname='/usr/share/fonts/truetype/cmu/cmunrm.ttf')
mpl.rcParams['font.family'] = prop.get_name()

class plot_temp:
	def __init__(self,x_mod,x_f_paper,x_i_paper,T_f_mod,T_f_paper,T_i_mod,T_i_paper):
		fig, axes = plt.subplots(1,2)

		''' Plot the fluid temperature'''
		axes[0].plot(x_mod,T_f_mod,label='Modelica')
		axes[0].plot(x_f_paper,T_f_paper,label='Sánchez-González [1]')
		axes[0].legend(loc='best',frameon=False)
		axes[0].set_xlabel(r"$z_\mathrm{fp} (m)$")
		axes[0].set_ylabel(r"$T_\mathrm{f} (\degree C)$")

		''' Plot the crown temperature'''
		axes[1].plot(x_mod,T_i_mod,label='Modelica')
		axes[1].plot(x_i_paper,T_i_paper,label='Sánchez-González [1]')
		axes[1].legend(loc='best',frameon=False)
		axes[1].set_xlabel(r"$z_\mathrm{fp} (m)$")
		axes[1].set_ylabel(r"$T_\mathrm{s} (\degree C)$")

		plt.autoscale(tight=True)
		plt.show()
