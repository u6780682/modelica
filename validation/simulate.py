# -*- coding: utf-8 -*-
from solartherm import simulation
import os
import shutil
import DyMat
import numpy as np
import xlsxwriter
from sys import path
from scipy.interpolate import interp1d,spline

import matplotlib as mpl
mpl.use('GtkAgg')
mpl.rcParams["figure.dpi"] = 100.
mpl.rcParams["figure.figsize"] = [11, 5]
mpl.rcParams['font.size'] = 10
import matplotlib.pyplot as plt
import matplotlib.font_manager as font_manager

'''Comparison of the wall and fluid temperatures. The source of 
   verification is the paper from the Univ. Carlos III (Madrid, Spain) titled:

   Sánchez-González, Alberto, María Reyes Rodríguez-Sánchez, and Domingo Santana. 
   "Aiming strategy model based on allowable flux densities for molten salt central receivers."
   Solar Energy 157 (2017): 1130-1144.
   https://doi.org/10.1016/j.solener.2015.12.055'''

prop = font_manager.FontProperties(fname='/usr/share/fonts/truetype/cmu/cmunrm.ttf')
mpl.rcParams['font.family'] = prop.get_name()

wd = path[0]

# Preparing the SolarTherm folder
#os.system('export OPENMODELICALIBRARY="/home/arfontalvo/.openmodelica/libraries:/home/arfontalvo/.local/lib/omlibrary:/usr/lib/omlibrary:/usr/local/lib/omlibrary"')
#os.system('rm -rf /home/arfontalvo/.local/lib/omlibrary/SolarTherm/')
#os.system('cd /home/arfontalvo/solartherm/; cmake -DCMAKE_INSTALL_PREFIX=/home/arfontalvo/.local; make -j4 install')

os.chdir(wd)

#par_n = ['vwind'] #Parameters name to update after compilation
#par_v = ['0'] #Parameters values to update after compilation

#sim = simulation.Simulator('%s/Verification.mo'%wd)
#sim.compile_model()
#sim.compile_sim(args=['-s'])
#sim.load_init()
#sim.update_pars(par_n,par_v)
#sim.simulate()

data = DyMat.DyMatFile('%s/Verification_res.mat'%wd)

dz = data.data('d_z')[0] #Receiver height (m)
N = int(data.data('N')[0]) #Number of panels per flowpath)

T_f_mod = [] #Temperature of the fluid along the flowpath
T_i_mod = [] #Temperature of the inner crown along the flowpath
sigma_theta = []
sigma_z = []
sigma_eq = []
for i in range(N): #iterating over the flowpath lenght
	T_f_mod.append(data.data('T_f[%s]'%(i+1))[0] - 273.15)
	T_i_mod.append(data.data('T_i[%s]'%(i+1))[0] - 273.15)
	sigma_theta.append(data.data('pipe[%s].sigma_theta'%(i+1))[0]/1e6)
	sigma_z.append(data.data('pipe[%s].sigma_z'%(i+1))[0]/1e6)
	sigma_eq.append(data.data('pipe[%s].sigma_eq'%(i+1))[0]/1e6)


x = np.linspace(0,dz*N,N)

data = np.genfromtxt('verification.csv',delimiter=",", skip_header=1)
xfd = data[:,6]
yfd = data[:,7]
xpd = data[:,4]
ypd = data[:,5]

xf = xfd[np.logical_not(np.isnan(xfd))]
yf = yfd[np.logical_not(np.isnan(yfd))]
xp = xpd[np.logical_not(np.isnan(xpd))]
yp = ypd[np.logical_not(np.isnan(ypd))]

p = interp1d(xp,yp,kind='cubic',bounds_error=False,fill_value=np.max(yp))
f = interp1d(xf,yf,kind='cubic',bounds_error=False,fill_value=np.max(yf))

T_i_paper = p(x)
T_f_paper = f(x)

error_i_paper = abs(p(x) - T_i_mod)/T_i_mod*100
error_f_paper = abs(f(x) - T_f_mod)/T_f_mod*100

# Creating 1 row and 2 column subplots
fig, axes = plt.subplots(2,2)

''' Plot the fluid temperature'''
# subplot 1,1 (row 1, col 1)
axes[0,0].plot(x,T_f_mod,label='Modelica')
axes[0,0].plot(xf,yf,label='Sánchez-González [1]')
axes[0,0].legend(loc='best',frameon=False)
axes[0,0].set_xlabel(r"$z_{\mathrm{fp}}$ (m)")
axes[0,0].set_ylabel(r"$T_{\mathrm{f}}$ (°C)")

axes[1,0].plot(x,error_f_paper,label='Fluid temperature')
axes[1,0].plot(x,error_i_paper,label='Film temperature')
axes[0,0].set_xlabel(r"$z_{\mathrm{fp}}$ (m)")
axes[1,0].set_ylabel('Absolute error (% °C/°C)')

''' Plot the crown temperature'''
# subplot 1,2 (row 1, col 2)
axes[0,1].plot(x,T_i_mod,label='Modelica')
axes[0,1].plot(xp,yp,label='Sánchez-González [1]')
axes[0,1].legend(loc='best',frameon=False)
axes[0,1].set_xlabel(r"$z_{\mathrm{fp}}$ (m)")
axes[0,1].set_ylabel(r"$T_{\mathrm{s}}$ (°C)")

axes[1,1].plot(x,sigma_theta,label='Circumferential')
axes[1,1].plot(x,sigma_z,label='Axial stress')
axes[1,1].plot(x,sigma_eq,label='Equivalent stress')
axes[1,1].legend(loc='best',frameon=False)
axes[0,0].set_xlabel(r"$z_{\mathrm{fp}}$ (m)")
axes[1,1].set_ylabel('Stress (MPa)')

plt.autoscale(tight=True)
plt.show()
