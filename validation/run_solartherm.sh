#!/bin/sh

cd ~/.local/lib/omlibrary/
rm -rf SolarTherm/
cd ~/solartherm/
cmake -DCMAKE_INSTALL_PREFIX=~/.local && make -j4 install
cd ~/solartherm/examples/
. ../src/env/st_local_env
cd ~/gitlab/receiver/validation
python master.py
st_deactivate
