model Verification
	import Modelica.Utilities.Streams.*;
	import Modelica.Utilities.Strings.*;
	import Modelica.SIunits.Conversions.*;
	import Modelica.Constants.*;
	import Modelica.Math.*;

	Modelica.SIunits.HeatFlux q0[N];
	Modelica.SIunits.Temperature T_f[N];
	Modelica.SIunits.Temperature T_i[N];
	Modelica.SIunits.Temperature T_s[N];

	String line;
	parameter String filepath = "/home/arfontalvo/gitlab/receiver/validation/uniform225.csv";
	String filename = Modelica.Utilities.Files.loadResource(filepath);
	parameter SI.Height H_receiver = 10.5 "Receiver height"; //Gemasolar solar power tower https://doi.org/10.1016/j.solener.2015.12.055
	parameter SI.Velocity vwind = 0;
	parameter Integer N = 225 "Number of segments";
	parameter Integer Npa = 9 "Number of panels per flow path";
	parameter Modelica.SIunits.Length d_z = H_receiver*Npa/N "Length of pipe segment";
	parameter Modelica.SIunits.Temperature Tin = from_degC(290);
	parameter Modelica.SIunits.MassFlowRate m_flow = 4.2;
	parameter Modelica.SIunits.CoefficientOfHeatTransfer h_ext = 30;
	//SolarTherm.Models.CSP.CRS.Receivers.Panel.PipeSection[N] pipe(each dz = d_z, each hc = h_ext);
	SolarTherm.Models.CSP.CRS.Receivers.Panel.PipeSection[N] pipe(each dz = d_z, each v_wind = vwind);

algorithm
	for i in 1:N loop
	line := readLine(filename,i);
	q0[i] := scanReal(line)*1000000;
	end for;

equation
	pipe[1:end].Ti = T_i[1:end];
	pipe[1:end].Ts = T_s[1:end];
	pipe[1:end].Tin = T_f[1:end];
	pipe[1:end].q0 = q0[1:end];
	pipe[1].Tin = Tin;
	pipe[2:end].Tin = pipe[1:(end-1)].Tout;
end Verification;
