# -*- coding: utf-8 -*-
from sys import path
import simulate
import plotting

'''Comparison of the wall and fluid temperatures. The source of 
	verification is the paper from the Univ. Carlos III (Madrid, Spain) titled:

	Sánchez-González, Alberto, María Reyes Rodríguez-Sánchez, and Domingo Santana.
	"Aiming strategy model based on allowable flux densities for molten salt central receivers."
	Solar Energy 157 (2017): 1130-1144.
	https://doi.org/10.1016/j.solener.2015.12.055'''

wd = path[0]
fn = 'Verification'
par_n = ['h_ext']
par_v = ['30']

simulate.stcompile(wd,fn)
simulate.simulate(wd,fn)
simulate.update_pars(wd,fn,par_n,par_v)
res = simulate.load_data(wd,fn)
res.remove_files(fn)
res.get_temps_mod()
res.get_temps_paper()
plotting.plot_temp(res.x_mod,res.x_f_paper,res.x_i_paper,res.T_f_mod,res.T_f_paper,res.T_i_mod,res.T_i_paper)
