# -*- coding: utf-8 -*-
import os
import shutil
import DyMat
import numpy as np
from scipy.interpolate import interp1d,spline
import matplotlib as mpl
mpl.use('GtkAgg')
mpl.rcParams["figure.dpi"] = 100.
mpl.rcParams["figure.figsize"] = [11, 5]
mpl.rcParams['font.size'] = 12
import matplotlib.pyplot as plt
import matplotlib.font_manager as font_manager

prop = font_manager.FontProperties(fname='/usr/share/fonts/truetype/cmu/cmunrm.ttf')
mpl.rcParams['font.family'] = prop.get_name()

num = [5]

fig, axes = plt.subplots(2,2)

for j in num:
	data = DyMat.DyMatFile('Verification_res_%s.mat'%(j))

	dz = data.data('d_z')[0] #Receiver height (m)
	N = int(data.data('N')[0]) #Number of panels per flowpath)

	T_f_mod = [] #Temperature of the fluid along the flowpath
	T_i_mod = [] #Temperature of the inner crown along the flowpath
	for i in range(N): #iterating over the flowpath lenght
		T_f_mod.append(data.data('T_f[%s]'%(i+1))[0] - 273.15)
		T_i_mod.append(data.data('T_i[%s]'%(i+1))[0] - 273.15)

	x = np.linspace(0,dz*N,N)

	axes[0,0].plot(x,T_f_mod,label=r'Number of $A_{n}$: %s'%(j))
	axes[0,1].plot(x,T_i_mod,label=r'Number of $A_{n}$: %s'%(j))

data = np.genfromtxt('verification.csv',delimiter=",", skip_header=1)

xfd = data[:,6]
yfd = data[:,7]
xpd = data[:,4]
ypd = data[:,5]

xf = xfd[np.logical_not(np.isnan(xfd))]
yf = yfd[np.logical_not(np.isnan(yfd))]
xp = xpd[np.logical_not(np.isnan(xpd))]
yp = ypd[np.logical_not(np.isnan(ypd))]

p = interp1d(xp,yp,kind='cubic',bounds_error=False,fill_value=np.max(yp))
f = interp1d(xf,yf,kind='cubic',bounds_error=False,fill_value=np.max(yf))

error_i_paper = abs(p(x) - T_i_mod)/T_i_mod*100
error_f_paper = abs(f(x) - T_f_mod)/T_f_mod*100

axes[0,0].plot(xf,yf,label="Sánchez-González [1]")
axes[0,0].legend(loc='best',frameon=False)
axes[0,0].set_xlabel(r"$z_{\mathrm{fp}}$ (m)")
axes[0,0].set_ylabel(r"$T_{\mathrm{f}}$ (°C)")

axes[1,0].plot(x,error_f_paper)
axes[1,0].set_ylabel('Absolute error (% °C/°C)')

axes[0,1].plot(xp,yp,label='Sánchez-González [1]')
axes[0,1].legend(loc='best',frameon=False)
axes[0,1].set_xlabel(r"$z_{\mathrm{fp}}$ (m)")
axes[0,1].set_ylabel(r"$T_{\mathrm{s}}$ (°C)")

axes[1,1].plot(x,error_i_paper)
axes[1,1].set_ylabel('Absolute error (% °C/°C)')

plt.autoscale(tight=True)
fig.tight_layout()
plt.show()
