import numpy as np
import pandas as pd
import matplotlib as mpl
from matplotlib import pyplot as plt
import os
import scipy
from scipy.interpolate import interp1d,spline

data = np.genfromtxt('verification.csv',delimiter=",", skip_header=1)
xp = data[:,0]
yp = data[:,1]

x = np.linspace(0,94.5,9*25)

f = interp1d(xp,yp,kind='cubic',bounds_error=False,fill_value=np.min(yp))

y = f(x)

np.savetxt('uniform.csv',y,delimiter=',')

#plt.plot(x, y,label='cubic', linewidth=2, color='#3465a4')
#plt.plot(xp, yp,'s',label='Sanchez-Gonzalez',linewidth=2, color='#cc0000')
#plt.legend(loc='best')
#plt.show()
